

--[[
	* Function returns count of the object's elements
	*
	* @T:	obj, object for counting
	*
	* @return: int, count of the elemetns in object
	*
	* @since  11.02.14
	* @author pcemma
]]
table.objectSize = function (T) --tablelength
	local count = 0
	if(T and type(T) == 'table') then		
		for _ in pairs(T) do count = count + 1 end	
	end
	return count
end


--[[
	* Function проевряет наличие эллемента в массиве
	*
	* @tbl:		array, в котором ищем нжный эллемент
	* @value:	?, любое значение любого типа
	*
	*
	* @return: 	bollean,  наличие эллемента в массиве
	*
	* @since  19.03,14
	* @author pcemma
]]
table.inArray = function(tbl, value)
	for k,v in pairs(tbl) do
		if value == v then
			return true
		end
	end
	return false
end


function table.clone(t)
	local t2 = {}
	for k,v in pairs(t) do
		t2[k] = v
	end
	return t2
end


--[[
	* Function переводит число (секунд) в строку таймера вида: 00:00:00:00 (д:ч:м:с)
	*
	* @nSeconds:	int, количество секнуд для таймера строки
	*
	* @return: 		string, строку вида 00:00:00:00 (д:ч:м:с)
	*
	* @since  19.02.14
	* @author pcemma
]]
string.timeFormat = function(nSeconds)
	local p1, p2 = pcall(function()
		nSeconds = tonumber(nSeconds);
		local nDays, nHours, nMins, nSecs = 0, 0, 0, 0;
		local fDays, fHours, fMins = true, true, true;
		local nString = "";
		
		if(nSeconds > 0)then
		
			nDays = string.format("%02.f", math.floor(nSeconds/86400));
			
			if(math.floor(nSeconds/86400) == 0) then
				fDays = false;
			else
				nString = nDays..":";
			end
			
			nHours = string.format("%02.f", math.floor((nSeconds - nDays * 86400) / 3600 ));
			if(math.floor((nSeconds - nDays * 86400) / 3600 ) == 0 and fDays == false) then
				fHours = false;
			else
				nString = nString..nHours..":";
			end
			
			
			nMins = string.format("%02.f", math.floor( (nSeconds - nDays * 86400 - nHours * 3600) / 60 ));
			if(math.floor( (nSeconds - nDays * 86400 - nHours * 3600) / 60 ) == 0 and fHours == false) then
				fMins = false;
				nString = nString..nMins..":";
			else
				nString = nString..nMins..":";
			end
			
			
			nSecs = string.format("%02.f", math.floor(nSeconds - nDays * 86400 - nHours * 3600 - nMins * 60));
			nString = nString..nSecs;
		
		else
			nString = "00:00";
		end
		
		
		return nString
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.timeFormat', p2);
	else
		return p2;
	end
end


--[[
	* Function переводит число (секунд) в количество минут/часов/дней в зависимости от количества секунд
	*
	* @nSeconds:	int, количество секнуд для таймера строки
	*
	* @return: 		string, строку вида 00 (д/ч/м/с)
	*
	* @since  16.04.14
	* @author pcemma
]]
string.timeFormat2 = function(nSeconds)
	local p1, p2 = pcall(function()
		nSeconds = tonumber(nSeconds);
		
		local nString = "";
		
		if(math.floor(nSeconds/86400) ~= 0) then
			nString = math.floor(nSeconds/86400);
		elseif(math.floor(nSeconds / 3600 ) ~= 0)	then
			nString = math.floor(nSeconds / 3600 );
		elseif(math.floor(nSeconds / 60 ) ~= 0) then
			nString = math.floor(nSeconds / 60 );
		else
			nString = math.floor(nSeconds);
		end
		
		return nString
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.timeFormat2', p2);
	else
		return p2;
	end
end



--[[
	* Function переводит число в сокращеный формат
	*
	* @count:	int, количество 
	*
	* @return: 		string, строку вида 000 (если число до 1000), 999К (если число до миллиона), 999М (если > миллиона)
	*
	* @since  19.04.14
	* @author pcemma
]]
string.numberFormat = function(count)
	local p1, p2 = pcall(function()
		count = math.floor(tonumber(count));
		local nString = "";
		local add = '';
		local dec = 2;
		if(count >= 1000000)then
			add = "M";
			if(count / 1000000 >= 100) then dec = 1; end
			nString = string.format("%."..dec.."f%s", round(count / 1000000, dec), add);
		elseif(count >= 1000)then
			add = "K";
			if(count / 1000 >= 100) then dec = 1; end
			nString = string.format("%."..dec.."f%s", round(count / 1000, dec), add);
		else
			nString = string.format("%.f%s", count, add);
		end
		return nString
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.numberFormat', p2);
	else
		return p2;
	end
end


--[[
	* Function: get string length
	*
	* @str:		str, string 
	* @limit:	int, limit if string kength 
	*
	* @return: 		string,
	*
	* @since  10.02.15
	* @author t
]]
string.length = function (str)
	local p1, p2 = pcall(function()
		local _, count = string.gsub(str, "[^\128-\193]", "")

		return count;
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.length', p2);
	else
		return p2;
	end	
end

--[[
	* Function: cut any str
	*
	* @str:		str, string 
	* @limit:	int, limit if string kength 
	*
	* @return: 		string,
	*
	* @since  10.02.15
	* @author t
]]
string.cut = function (str, limit, reverse)
	local p1, p2 = pcall(function()
		local code = "";
		local i = 1;
		local j = 1;
		local s = "";
		str = str or ""; 
		limit = limit or 1;

		if not reverse then
			for code in str:gmatch("[%z\1-\127\194-\244][\128-\191]*") do
				s = s..code;
				i=i+1;
				if i > limit then
					break;
				end	
			end
		else
			for code in str:gmatch("[%z\1-\127\194-\244][\128-\191]*") do
				if i > limit then
					s = s..code;
				end
				i=i+1;
			end	
		end
		return s;
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.cut', p2);
	else
		return p2;
	end	
end


--[[
	* Function: string trim
	*
	* @str:		str, string 
	*
	* @return: 		string,
	*
	* @since  26.02.15
	* @author t
]]
string.trim = function (s)
	local p1, p2 = pcall(function()
		return (s:gsub("^%s*(.-)%s*$", "%1"))
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('string.trim', p2);
	else
		return p2;
	end	
end


--[[
	* Function get date
	*
	*
	* @return: 	str,  date
	*
	* @since  16.01.15
	* @author t
]]
function getDate(timestamp)
	local p1, p2 = pcall(function()
		-- Server Time +3600 to the Pidorussian Time
		local d = os.date( "*t",  timestamp + 3600);
		return d.month.."/"..d.day.."/"..d.year.." "..string.format("%02.f", d.hour)..":"..string.format("%02.f", d.min);
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('distanceBetweenPoints', p2);
	else
		return p2;
	end
end


--[[
	* Function считает расстояние между двумя точками в пространстве
	*
	* @a:		array, точка, с координатами х и у
	* @b:		array, точка, с координатами х и у
	*
	*
	* @return: 	int,  расстояние между 2 точками a и b
	*
	* @since  27.02.14
	* @author pcemma
]]
function distanceBetweenPoints( a, b )
	local p1, p2 = pcall(function()
		local width, height = b.x-a.x, b.y-a.y;
		return (width*width + height*height)^0.5;
	end);
	if(p1 == false) then
		networkManager:send_client_error_to_server('distanceBetweenPoints', p2);
	else
		return p2;
	end
end


function print_r ( t ) 
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
						print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    sub_print_r(t,"  ")
end


function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult ) / mult
end


--[[
	function return number of bytes in string in format 00000
	str - string
]]
function return_bytes(str)
	local byte_num = tostring(#str);
	while string.len(byte_num) < 6 do
		byte_num = '0'..byte_num;
	end
	return byte_num;
end


function Sort( aMap ) 
	local aList = {} 
	local anIndex = 1 
	
	for aKey, aValue in pairs( aMap ) do
		aList[ #aList + 1 ] = aKey 
	end 
	
	table.sort( aList ) 
	
	return function() 
		if(aList and anIndex)then
			local aKey = aList[ anIndex ] 
			local aValue = aMap[ aKey ] 
		
			anIndex = anIndex + 1 
			return aKey, aValue 
		end
	end 
end


function SortAsNum( aMap ) 
	local aList = {} 
	local anIndex = 1 
	
	for aKey, aValue in pairs( aMap ) do
		aList[ #aList + 1 ] = tonumber(aKey); 
	end 
	
	table.sort( aList ) 
	
	return function() 
		local aKey = aList[ anIndex ] 
		local aValue = aMap[ tostring(aKey) ] 
		
		anIndex = anIndex + 1 
		
		return aKey, aValue 
	end 
end


--[[
	* Function сортирует обьект по заданному правилу, используется в циклах
	* Если не задавать правило, сортировка будет проходить по ключу, иначе - по свойству обьекта
	*
	* @aMap:	object, обьект, который нужно отсортировать
	* @rule*:	function, правило, по которому идет сортировка
	*
	*			* - не обязательный параметр
	*
	* @return: 	key, value
	*
	* @since  21.05.14
	* @author t
]]
function sortObject( aMap , rule)
	local aList = {} 
	local anIndex = 1 
	
	for aKey, aValue in pairs( aMap ) do
		table.insert(aList, { key = aKey, value = aValue })
	end 
	
	rule = rule or function(x, y) return x.key < y.key; end

	table.sort( aList, rule ) 

	return function() 
		if( aList and anIndex and aList[ anIndex ] )then
		
			local aKey = aList[ anIndex ].key
			local aValue = aMap[ aKey ] 

			anIndex = anIndex + 1 
			return aKey, aValue 
		end
	end
end


function calculateAngle( sideA, sideB )
   local cosx = math.abs( sideA ) / math.abs( sideB )
   -- local cosx = sideA / sideB 
   local acosx = math.acos( cosx )  -- result in radians
   local angle = acosx * 180 / math.pi  -- converted to degrees   
   return angle
end	
