-- Function to retrieve a widget's theme settings
local function _getTheme( widgetTheme, options )	
	local theme = nil
	-- If a theme has been set
	if widget.theme then
		theme = widget.theme[widgetTheme]
	end
	
	-- If a theme exists
	if theme then
		-- Style parameter optionally set by user
		if options and options.style then
			local style = theme[options.style]
			
			-- For themes that support various "styles" per widget
			if style then
				theme = style
			end
		end
	end
	
	return theme
end

local function createWidget(createFunction, ...)

  local defAnchorX, defAnchorY
  if not isGraphicsV1 then

    defAnchorX = display.getDefault( "anchorX")
    defAnchorY = display.getDefault( "anchorY" )
    widget._oldAnchorX = defAnchorX
    widget._oldAnchorY = defAnchorY

    display.setDefault( "anchorX", 0.5)
    display.setDefault( "anchorY", 0.5 )

  end
  local w = createFunction(...)
  if not isGraphicsV1 then

    display.setDefault( "anchorX", defAnchorX)
    display.setDefault( "anchorY", defAnchorY )
    w.anchorX = defAnchorX
    w.anchorY = defAnchorY 
  end
  return w
end

function newButton( options )	
	local theme = _getTheme( "button", options )
	local _button = require( "lib.widgets.widget_button" )
	return createWidget(_button.new, options, theme )
end

function newScrollView( options )	
	local theme = _getTheme( "scrollView", options )
	local _scrollView = require( "lib.widgets.widget_scrollview" )
	return createWidget(_scrollView.new, options, theme )
end

