print("CLASS GameDesignClass IS CONNECTED");
local GameDesignClass = {};

function GameDesignClass.new(params)
	
	local self = RootClass.new({});
	
	--[[
		* Function change coordinats and/or scaling to object and other params
		*	@obj:			obj, the object to change
		*	@params: 			array;
		* 	[
		*		@x 				- round, x coordinate of obj
		*		@y				- round, y coordinate of obj
		*		@scaleType		- type of the scale ("x", "y")
		*		@scale_x,		- round, x scale koef,
		*		@scale_y,		- round, y scale koef,
		* 		@alpha:			- number, the alpha for object;
		* 		@txt_size:		- number, the the text size for text objects;
		* 		@color:			- array, array of the colors for object;
		* 		@anchorX		- int, x anchor
		* 		@anchorY		- int, y anchor
		* 	
	]
		
		
		* @since  13.02.2014
		* @author pcemma
	]]
	function self:changeObject(obj, params)
		local p1, p2 = pcall(function()
			-- REFERENCE POINT	
			params.anchor_x = params.anchor_x or 0.5;
			params.anchor_y = params.anchor_y or 0.5;
			
			obj.anchorChildren = true;
			
			obj.anchorX = params.anchor_x;
			obj.anchorY = params.anchor_y;
		
			
			-- ALPHA	
			obj.alpha = params.alpha or 1;
			
			-- SCALE
			local koefX = 1;
			local koefY = 1;
			
			local scaleX, scaleY = obj.contentWidth, obj.contentHeight;
			
			if params.scale then
				scaleX = params.scale;
				scaleY = params.scale;			
				
				if params.scaleType == "x" then
					koefX = 1;
					koefY = obj.contentHeight / obj.contentWidth;
				else
					koefX = obj.contentWidth / obj.contentHeight;
					koefY = 1;
				end
			else
				if params.scale_x then
					scaleX = params.scale_x;
					koefX = 1;
				end
				
				if params.scale_y then
					scaleY = params.scale_y;
					koefY = 1;
				end
			end
			
			obj.xScale = scaleX*koefX / obj.contentWidth
			obj.yScale = scaleY*koefY / obj.contentHeight
			
			
			-- X,Y COORDINATES	
			obj.x = params.x or 0;
			obj.y = params.y or 0;
			
			if(params.color)then
				for i=1,3 do
					if not params.color[i] then
						params.color[i] = 0;
					end
					if params.color[i] > 1 then
						params.color[i] = params.color[i]/255;
					end
				end
				obj:setFillColor(params.color[1],params.color[2],params.color[3]);
			end
			
			if params.rotate then
				obj:rotate(params.rotate);
			end
			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:changeObject', p2);
		else
			return p2;
		end
	end

	
	--[[
		* Function: Function создания расширеной группы с методом очистки
		
		* @params:	
		
		
		
		* @since  18.06.14
		* @author pcemma
	]]
	function self:createGroup()
		local p1, p2 = pcall(function()
			local group = display.newGroup();
			
			
			function group:clear()
				local p1, p2 = pcall(function()
					-- clear the contents of a group, but don't delete the group
					if(self ~= nil) then
						if(type(self.numChildren) == 'number') then
							for i=self.numChildren,1,-1 do
								display.remove( self[i] );
								self[i] = nil;		
							end
						end
					end
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createGroup:clear', p2);
				else
					return p2;
				end
			end	
			
			
			return group;
			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createGroup', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that create an image
		* @params:	array, array of params
		*	[
		*		@file 			- str, path to an image, or userdata, an image sheet
		*		@file 			- userdata, an image sheet
		
		*		@group			- obj, group to inserting
		*		@frame_number	- int, number of current frame in image sheet
		*		@x 				- round, x coordinate of obj
		*		@y				- round, y coordinate of obj
		*		@alpha			- round, alpha of obj,
		*		@scale_x,		- round, x scale koef,
		*		@scale_y,		- round, y scale koef,
		*		@scaleType		- type of the scale ("x", "y")
		* 		@color:			- array, array of the colors for object;
		* 		@anchorX		- int, x anchor
		* 		@anchorY		- int, y anchor
		*	]
	]]
	function self:createImage(params)
		local p1, p2 = pcall(function()
			local image;
			if(params.frame_number)then
				image = display.newImage( params.file, params.frame_number, true );
			else
				image = display.newImage( params.file, _IMAGES_DERICTORY, true );
			end
			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(image);
			end
			
			-- change object
			self:changeObject(image, params);

			-- RESULT
			return image;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createImage', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that create a rect
		* @params:	array, array of params
		*	[
		*
		*		@group			- obj, group to inserting
		*		@x 				- round, x coordinate of obj
		*		@y				- round, y coordinate of obj
		*		@alpha			- round, alpha of obj,
		*		@scale_x,		- round, x scale koef,
		*		@scale_y,		- round, y scale koef,
		*		@scaleType		- type of the scale ("x", "y")
		* 		@color:			- array, array of the colors for object;
		* 		@anchor_x		- round, x anchor
		* 		@anchor_y		- round, y anchor
		* 		@width			- round, width of rect
		* 		@height			- round, height of rect
		*	]
	]]
	function self:createRect(params)
		local p1, p2 = pcall(function()
			local rect;
			
			-- SET VARIABLES
			params.x = params.x or _W/2
			params.y = params.y or _H/2
			
			params.width = params.width or _W;
			params.height = params.height or _H;
			
			-- REFERENCE POINT	
			params.anchor_x = params.anchor_x or 0.5;
			params.anchor_y = params.anchor_y or 0.5;
			
			rect = display.newRect( params.x,params.y, params.width, params.height );

			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(rect);
			end
			
			-- change object
			self:changeObject(rect, params);

			-- RESULT
			return rect;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createRect', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that create an text
		* @params:	array, array of params
		*	[
		*		@txt			- str,	a title for new text
		*		@group			- obj, group to inserting
		*		@x 				- round, x coordinate of obj
		*		@y				- round, y coordinate of obj
		*		@alpha			- round, alpha of obj,
		*		@txt_size		- int, text size
		* 		@anchorX		- int, x anchor
		* 		@anchorY		- int, y anchor
		* 		@color:			- array, array of the colors for object;
		*		@font:			- str, type of font
		*	]
	]]
	function self:createTitle(params)
		local p1, p2 = pcall(function()
			local title;
			-- local game_font = native.systemFont
			local gameFont = params.font or gameFont;

			-- text for title
			if(params.txt == nil) then
				params.txt = '';
			end
			
			--size
			if(params.txt_size == nil) then
				params.txt_size = 14;
			end
			
			--width, height of text
			if(params.width == nil) then
				params.width = 0;
			end
			if(params.height == nil) then
				params.height = 0;
			end
			
			local options = {
				text = params.txt,     
				x = params.x,
				y = params.y,
				width = params.width,     --required for multi-line and alignmentD
				height = params.height,     --required for multi-line and alignmentD
				font = gameFont,   
				fontSize = params.txt_size,
				align = params.align  --new alignment parameter		
			}
			title = display.newText( options );
			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(title);
			end
			
			-- change object
			self:changeObject(title, params);

			-- RESULT
			return title;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createTitle', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that create background with empty listner and not touch!!
		* @params:	array, array of params
		*	[
		*		@group			- obj, group to inserting
		*		@x 				- round, x coordinate of obj
		*		@y				- round, y coordinate of obj
		*		@alpha			- round, alpha of obj,	
		*		@width,			- round, width of the rect,
		*		@height,		- round, height of the rect
		* 		@anchor_x		- int, x anchor
		* 		@anchor_y		- int, y anchor
		* 		@color:			- array, array of the colors for object;
		*	]
	]]
	function self:createBgNotTouch(params)
		local p1, p2 = pcall(function()
			local obj;
			-- SET VARIABLES
			params.x = params.x or _W/2
			params.y = params.y or _H/2
			
			params.width = params.width or _W;
			params.height = params.height or _H;
			
			-- REFERENCE POINT	
			params.anchor_x = params.anchor_x or 0.5;
			params.anchor_y = params.anchor_y or 0.5;

			obj = display.newRect(params.x, params.y, params.width, params.height);
			
			-- alpha
			obj.alpha = params.alpha or 0.01;
			
			-- color
			if(params.color)then
				obj:setFillColor(params.color);
			end
			
			obj.anchorX = params.anchor_x;
			obj.anchorY = params.anchor_y;		
			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(obj);
			end
			
			-- change object
			-- self:changeObject(obj, params);
			
			obj:addEventListener ("touch", function(event) return true; end);
			
			-- RESULT
			return obj;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createBgNotTouch', p2);
		else
			return p2;
		end
	end


	--[[
	CREATE WIDGET newTableView
		params : array;
		{
			params.top - coordinate y of widget
			params.left - coordinate x of widget
			params.width - width of widget
			params.height - height of widget,
			params.hideScrollBar
			params.scrollWidth
			params.scrollHeight
			params.listener
		}
	]]
	function self:createScrollView(params)
		local p1, p2 = pcall(function()

			local scrollView;

			if(params.hideScrollBar == '' or params.hideScrollBar == nil) then
				params.hideScrollBar = true;

			end
			
			if(params.hideBackground == '' or params.hideBackground == nil) then
				params.hideBackground = true;

			end		
			
			if params.horizontalScrollDisabled == nil then
				params.horizontalScrollDisabled = true;
			end
			
			if params.verticalScrollDisabled == nil then
				params.verticalScrollDisabled = false;
			end
			
			-- Create image sheet for custom scroll bar
			local scrollBarOpt = {
				width = 20,
				height = 20,
				numFrames = 3,
				
				sheetContentWidth = 60,
				sheetContentHeight = 20
			}
			local scrollBarSheet = graphics.newImageSheet( "images/windowInterface/scrollBar.png", scrollBarOpt )
		
			-- scrollView = widget.newScrollView{
			scrollView = newScrollView{
				top = params.top, left = params.left,
				x = params.x, y = params.y,
				noLines = true,
				width = params.width, 
				height = params.height,
				scrollWidth = params.scrollWidth,
				scrollHeight = params.scrollHeight,
				backgroundColor = { 1, 0, 0, 0.5}, 	--Un-Comment this to set the background color of the scrollView.
				hideBackground = params.hideBackground,
				
				scrollBarOptions = {
					sheet = scrollBarSheet,
					topFrame = 1,
					middleFrame = 2,
					bottomFrame = 3
				},			
				
				horizontalScrollDisabled = params.horizontalScrollDisabled,
				verticalScrollDisabled = params.verticalScrollDisabled,
				-- scrollBarColor = { 0, 0, 1 }, --Un-Comment this to set the scrollBar to a custom color.
				hideScrollBar = params.hideScrollBar, 			--Un-Comment this to hide the scrollBar
				listener = params.listener or function(event) end
			}
			return scrollView;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createScrollView', p2);
		else
			return p2;
		end
	end


	--[[
	CREATE WIDGET newTableView
		params : array;
		{
			params.top - coordinate y of widget
			params.left - coordinate x of widget
			params.width - width of widget
			params.height - height of widget,
			params.hideScrollBar
			params.scrollWidth
			params.scrollHeight
			params.listener
		}
	]]
	function self:createSwitch(params)
		local p1, p2 = pcall(function()

			local switch;
			
			switch = widget.newSwitch{
				left = params.left,
				top = params.top,
				x = params.x,
				y = params.y,
				style = params.style,
				id = params.id,
				initialSwitchState = false,
				sheet = params.sheet,
				frameOff = params.frameOff,
				frameOn = params.frameOn,			
				onPress = params.onPress,
				onRelease = params.onRelease,
				onEvent = params.onEvent,
			}

			switch:setState( { isOn=params.isOn } );
			return switch;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createSwitch', p2);
		else
			return p2;
		end
	end


	--[[

		params : array;
		{
			x = _W*0.05, 
			y = _W*0.1, 
			style = "checkbox", 
			isOn = false,
			sheet = self.checkboxSheet,
			frameOff = 1,
			frameOn = 2,
			onRelease = function(event) end
		}
	]]
	function self:createCheckbox(params)
		local p1, p2 = pcall(function()
			params.type = params.type or "";
			local options = {
				width = 64,
				height = 64,
				numFrames = 2,
				sheetContentWidth = 128,
				sheetContentHeight = 64
			}
			local sheet = graphics.newImageSheet( "images/buttons/checkbox"..params.type..".png", options )
				
			local sequenceData =
			{
				{ name="on", frames={ 2 } },
				{ name="off", frames={ 1 } }
			}

			local checkbox = display.newSprite( sheet, sequenceData )
			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(checkbox);
			end
			
			--[[
				* Function: set checkbox state
				*
				* 	@params: isOn - bool
				*	
				*
				*
				* @since  16.01.15
				* @author t
			]]
			function checkbox:setState(data)
				local p1, p2 = pcall(function()
					if data.isOn then
						self.isOn = data.isOn;
						self:setSequence( "on" );
						self:play();
					else
						self.isOn = data.isOn;
						self:setSequence( "off" );
						self:play();				
					end
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createCheckbox:setState', p2);
				else
					return p2;
				end
			end		
		

			--[[
				* Function: touch listener
				*
				*	
				*
				*
				* @since  16.01.15
				* @author t
			]]
			function checkbox:touch(event)
				local p1, p2 = pcall(function()
					if event.phase == "began" then
						display.getCurrentStage():setFocus (event.target);
					elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
						-- проигрываем звук
						musicManager:playSound({file = musicManager.sounds.buttonClick});
						display.getCurrentStage():setFocus (nil);
					
						self:setState({ isOn = not self.isOn });
						params.onRelease(event);
					
						return true;
					end	
					return true;			
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createCheckbox:touch', p2);
				else
					return p2;
				end
			end		
		
			checkbox:addEventListener("touch", checkbox);
			checkbox:setState( { isOn=params.isOn } );
			
			if not params.scale and not params.scale_x and not params.scale_y then
				params.scale = _W * 0.05; 
				params.scaleType = "x";		
			end
			
			self:changeObject(checkbox, params);
			
			return checkbox;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createCheckbox', p2);
		else
			return p2;
		end
	end


	--[[
		CREATE WIDGET Sliser
		
		params : array;
		{
			params.top - coordinate y of widget
			params.left - coordinate x of widget
			params.width - width of widget
			params.height - height of widget,
			params.orientation
			params.value
			params.listener
		}
	]]
	function self:createSlider(params)
		local p1, p2 = pcall(function()

			local slider;
			local p = {
						top = params.top or 0,
						left = params.left or 0,
						width = params.width,
						height = params.height,
						x = params.x,
						y = params.y,
						value = params.value or 0,  -- Start slider at 0% (optional)
						
						
						listener = params.listener or function() end
					}
				
			local slider = widget.newSlider(params)

			
			return slider;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createSlider', p2);
		else
			return p2;
		end
	end


	--[[
	CREATE WIDGET newButton()
		* Function: создает кнопку 
		*
		* 	
		*	
		*
		*
		* @since  24.03.14
		* @author pcemma
	]]
	function self:createButton(params)
		local p1, p2 = pcall(function()
			local label = "";
			if(params.needLabel == nil) then
				label = params.title or "Default";
			end
			
			local button;
			
			
			if(params.sheet)then
				-- используя image sheet
				button = newButton{
					sheet = params.sheet,
					defaultFrame = params.defaultFrame,
					overFrame = params.overFrame,
					
					label = label,
					-- emboss = true,
					labelAlign = "center",
					labelAlignText = params.labelAlignText or "center",
					labelWidth = params.labelWidth or 0,
					labelHeight = params.labelHeight or 0,
					font = gameFont,
					labelYOffset = params.labelY or 0,
					labelXOffset = params.labelX or 0,
					fontSize  = params.fontSize or 35,
					labelColor = params.labelColor or { default={ 1, 1, 1 }, over = { 0, 0, 0, 0.5 } },
					onEvent = params.onEvent or function() end
				}
			else
				-- из просто картинок
				button = newButton{
					defaultFile = params.defaultFile,
					overFile = params.overFile,
					
					-- left = params.x or 0,
					-- top = params.y or 0,
					-- id = "button1",
					label = label,
					-- emboss = true,
					labelAlign = "center",
					labelAlignText = params.labelAlignText or "center",
					labelWidth = params.labelWidth or 0,
					labelHeight = params.labelHeight or 0,
					font = gameFont,
					labelYOffset = params.labelY or 0,
					labelXOffset = params.labelX or 0,
					fontSize  = params.fontSize or 35,
					labelColor = params.labelColor or { default={ 1, 1, 1 }, over = { 0, 0, 0, 0.5 } },
					onEvent = params.onEvent or function() end
				}
			end
			
			
			
			-- GROUP INSERTING	
			if params.group then
				params.group:insert(button);
			end
			
			
			self:changeObject(button, params);
			
			
			--[[
				* Function: заливает кнопку вместе с текстом цветом
				*
				* 	@params: array of color
				*	
				*
				*
				* @since  11.04.14
				* @author pcemma
			]]
			function button:setColor(params)
				local p1, p2 = pcall(function()
					for i=1,self.numChildren do
						self[i]:setFillColor(params[1] or 1, params[2] or 1, params[3] or 1);
					end
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createButton:setColor', p2);
				else
					return p2;
				end
			end
			
			
			--[[
				* Function: делает кнопку активной или не активной
				*
				* 	@state:	boolean, state for button
				*	
				*
				*
				* @since  12.05.14
				* @author pcemma
			]]
			function button:enable(state)
				local p1, p2 = pcall(function()
					
					if(state)then 
					-- включаем кнопку	
						self:setColor({1, 1, 1});					
						self._view._label:setFillColor(self._view._label._labelColor.default[1] or 1, self._view._label._labelColor.default[2] or 1, self._view._label._labelColor.default[3] or 1, 1);
						self._view._label.alpha = 1;
						for i=1,self.numChildren do
							if(self[i].text)then
								self[i]:setFillColor(self._view._label._labelColor.default[1] or 1, self._view._label._labelColor.default[2] or 1, self._view._label._labelColor.default[3] or 1, 1);
								self[i].alpha = 1;
							end
						end
					else
					-- выключаем кнопку
						self:setColor({.5,.5,.5});
						self._view._label.alpha = 1;
						for i=1,self.numChildren do
							if(self[i].text)then
								self[i].alpha = 1;
							end
						end
					end
					
					self:setEnabled( state )
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createButton:enable', p2);
				else
					return p2;
				end
			end
			
			
			--[[
				* Function: создает текст и картинку с ценой
				*
				* @params:
				*	@price:		int, price of the item 
				*	@type:		str, type of the price (gold, loyalty, talants)
				*	
				*
				*
				* @since  24.09.14
				* @author pcemma
			]]
			function button:createPriceGroup(params)
				local p1, p2 = pcall(function()
					-- 47 - это истинный рахмер иконки мнетки по ширине.
					params.price = params.price or " ";
					params.type = params.type or "gold";
					local groupX = params.startX or 185.2;
					local groupY = params.startY or button._view.y;
					button.priceImage = GameDesignClass:createImage({	
														file = "images/itemDiv/"..params.type.."PriceImage.png",
														group = button,
														scale = _W * 0.068, 
														-- scale = 47, 
														scaleType = "x", 
														anchor_x = 1,
														anchor_y = 0.5,
														y = button._view.y,
														x = button._view.x + groupX
													});
					button.priceTitle = GameDesignClass:createTitle({
														txt = params.price,
														group = button,
														txt_size = 29,
														anchor_x = 1,
														anchor_y = 0.5,
														color = {0, 0, 0},
														y = button._view.y,
														x = button.priceImage.x - 47});
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createButton:createPriceGroup', p2);
				else
					return p2;
				end
			end
			
			
			--[[
				* Function: Изменяет цену в свойстве цена если такого установлено
				*
				* @params:
				*	@price:		int, price of the item 
				*	
				*
				*
				* @since  24.09.14
				* @author pcemma
			]]
			function button:changePriceGroup(params)
				local p1, p2 = pcall(function()
					params.price = params.price or " ";
					if(button.priceTitle and button.priceTitle.alpha ~= nil)then
						button.priceTitle.text = params.price;
					end
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createButton:changePriceGroup', p2);
				else
					return p2;
				end
			end
			
			return button;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createButton', p2);
		else
			return p2;
		end
	end


	--[[
		CREATE Input
		
		params : array;
		{
			params.x		- coordinate y of Input
			params.y 		- coordinate x of Input
			params.width 	- width of Input
			params.height 	- height of Input,
			params.fontSize - font size in input
			params.type		- type of input: "default", "number" ...
			params.isSecure	- bool, secure text in input
			params.placeholder	- bool, secure text in input
			
			params.listener
		}
	]]
	function self:createInput(params)
		local p1, p2 = pcall(function()
			local isAndroid = "Android" == system.getInfo( "platformName" )
			params.fontSize = params.fontSize or 18
			params.height = params.height or 50
			params.width = params.width or 200

			if ( isAndroid ) then
				params.fontSize = params.fontSize - 4
				params.height = params.height + 10
			end
			
			local defaultField = {};
			
			local y = params.y
			if params.retranslator then
				y = -_H;
			end
			
			if system.getInfo( "environment" ) == "simulator" then
				defaultField = display.newRect( params.x, y, params.width, params.height );
				defaultField:setFillColor(0,0,0)
			else
				-- INPUT
				defaultField = native.newTextField( params.x, y, params.width, params.height );
				if params.placeholder then
					defaultField.placeholder = params.placeholder
				end
				defaultField.font = native.newFont( gameFont, params.fontSize );
				defaultField:setReturnKey("done")
				
				-- задаем тип инпута
				if(params.type and params.type ~= "")then
					defaultField.inputType = params.type;
				end
				
				-- TODO исправить Тип пароль!!!
				-- для типа пароль!!!
				if(params.isSecure)then
					defaultField.isSecure = params.isSecure;
				end
			end			
			
			-- LISTENER
			params.listener = params.listener or function(event) end;
			defaultField:addEventListener( "userInput", params.listener )
			
			if params.retranslator then
				params.input = defaultField;			
				defaultField.retranslator = self:createInputRetranslator(params);
			end
			
			return defaultField;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createInput', p2);
		else
			return p2;
		end
	end


	--[[
		CREATE Input
		
		params : array;
		{
			params.group
			params.x		
			params.y 		
			params.width
			params.txt,
			params.txt_size
			params.color
			params.input
		}
	]]
	function self:createInputRetranslator(params)
		local p1, p2 = pcall(function()

			local retranslator = display.newGroup();
			retranslator.isSecure = params.input.isSecure;

			-- GROUP INSERTING	
			if params.group then
				params.group:insert(retranslator);
			end
			
			params.height = params.height or CONSTANT.inputRetranaltorHeight;
			
			params.txt_size = params.txt_size or 29*CONSTANT.textScale;
			retranslator.flag = false;
			retranslator.w = params.width;
			
			if(not params.bgFrontColor)then
				params.bgFrontColor = {0.098, 0.101, 0.137};
			end
			if(not params.bgBackColor)then
				params.bgBackColor = {0.345, 0.352, 0.462};
			end
				
			retranslator.bgBack = self:createRect({
														color = params.bgBackColor,
														group = retranslator,
														width = params.width,
														height = params.height, 
														anchor_x = 0.5,
														anchor_y = 0.5,
														y = params.y,
														x = params.x
													});

			retranslator.bgFront = self:createRect({
														color = params.bgFrontColor,
														group = retranslator,
														width = params.width-2,
														height = params.height - 2, 
														anchor_x = 0.5,
														anchor_y = 0.5,
														y = params.y,
														x = params.x
													});

			retranslator.txt = self:createTitle({
													txt = params.txt,
													txt_size = params.txt_size,
													color = params.color,
													group = retranslator,
													anchor_x = 0,
													anchor_y = 0.5,
													y = params.y,
													x = params.x - (params.width) * 0.5
												});
			params.placeholder = params.placeholder or "";
			retranslator.placeholder = self:createTitle({
													txt = params.placeholder,
													txt_size = params.txt_size,
													color = {100,100,100},
													group = retranslator,
													anchor_x = 0.5,
													anchor_y = 0.5,
													y = params.y,
													x = params.x
												});											

			retranslator.blinkStripe = display.newRect(retranslator,0, retranslator.txt.y, 1, retranslator.txt.contentHeight*1.1)
			retranslator.blinkStripe.anchorX = 0;
			retranslator.blinkStripe.x = retranslator.txt.x + retranslator.txt.contentWidth;
			retranslator.blinkStripe.alpha = 0;
			
			function retranslator:hide()
				local p1, p2 = pcall(function()
					transition.cancel(self.blinkStripe.transition);
					self.blinkStripe.alpha = 0;
					self.flag = false;
					if self.txt.text == "" then
						self.placeholder.alpha = 1;
					else
						self.placeholder.alpha = 0;
					end				
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createInputRetranslator retranslator:hide', p2);
				else
					return p2;
				end			
			end

			
			function retranslator:setText(text)
				local p1, p2 = pcall(function()
					if not text then
						text = "";
					end
					
					-- TODO исправить Тип пароль!!!
					-- if(retranslator.isSecure)then
						-- local strLen = string.len (text);
						-- text = "";
						-- for i = 1, strLen do
							-- text = text.."*";
						-- end
					-- end
					self.txt.allText = text;				
					self.txt.text = text;

					-- CUT LEFT SIDE OF TEXT IF IT MORE THEN RETRANSLATOR WIDTH
					if( self.txt.contentWidth > self.w - 2 )then
						while  self.txt.contentWidth > self.w - 2 do
							self.txt.text = string.cut(self.txt.text, 1, true);
						end
					end
					
					self.blinkStripe.x = self.txt.x + self.txt.contentWidth;
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createInputRetranslator retranslator:setText', p2);
				else
					return p2;
				end			
			end	

		
			function retranslator:tap()
				local p1, p2 = pcall(function()
					if not self.flag then
						self.flag = not self.flag;
						self.placeholder.alpha = 0;
						self.blinkStripe.alpha = 1;
						self.blinkStripe.transition = transition.blink( self.blinkStripe, { time=1000 } )
						native.setKeyboardFocus( params.input ); 
					end
				end);
				if(p1 == false) then
					networkManager:send_client_error_to_server('GameDesignClass:createInputRetranslator retranslator:tap', p2);
				else
					return p2;
				end					
			end
			
			retranslator:addEventListener("tap", retranslator );
			
			return retranslator;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('GameDesignClass:createInputRetranslator', p2);
		else
			return p2;
		end
	end

	
	return self;
end	
	
return GameDesignClass;	