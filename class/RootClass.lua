print("CLASS RootClass IS CONNECTED");
local RootClass = {};


function RootClass.new(params)
	local self = {};
	self.deviceToken = "";
	
	--[[
		* Function: CONSTRACT
		*
		*	
		*
		*
		* @since  28.03.15
		* @author t
	]]
	function self:initRuntimeListeners()
		local p1, p2 = pcall(function()
	
			-- stop all local notifications
			self:removeLocalNotifications();
			
			-- KEY LISTENER
			Runtime:addEventListener( "key", self.onKeyEvent );	
			
			-- SUSPEND and RESUME LISTENER	
			Runtime:addEventListener( "system", self.onSystemEvent );
			
			-- ON NOTIFICATION LISTENER
			Runtime:addEventListener( "notification", self.onNotification );

		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:initRuntimeListeners', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: Удаления объекта 
		
		* @params:	
		
		
		
		* @since  08.02.14
		* @author pcemma, tooreckiy
	]]
	function self:remove()
		local p1, p2 = pcall(function()
			-- очищаем интерфейс
			if self then
				if self.interface then
					for i,v in pairs(self.interface) do
						display.remove(self.interface[i]);
						self.interface[i] = nil;
					end
				end
				-- очищаем все остальные данные
				for i,v in pairs(self) do
					display:remove(self[i]);
					self[i] = nil;
				end
			end
			self = nil;
			collectgarbage();
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:remove', p2);
		else
			return p2;
		end
	end


	--[[
		* Function: Очистка объекта интерфейса
		
		* @params:	
		
		
		
		* @since  08.02.14
		* @author pcemma, tooreckiy
	]]
	function self:clear()
		local p1, p2 = pcall(function()
			-- очищаем интерфейс
			if self then
				if self.interface then
					for i,v in pairs(self.interface) do
						if(self.interface[i] and self.interface[i].alpha ~= nil) then
							display.remove(self.interface[i]);
						end
						self.interface[i] = nil;
					end
					
					self.interface = nil;
				end
			end
			collectgarbage();
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:clear', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that check isset object!
		* @object - is an object that need to check
		
		* @since  04.02.2014
		* @author pcemma, tooreckiy
	]]
	function self:checkObject(object)
		local p1, p2 = pcall(function()
			if object and object.alpha ~= nil and table.objectSize(object) > 0 then
				return true
			else
				return false
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:checkObject', p2);
		else
			return p2;
		end
	end


	--[[
		* Function: clear  group object
		*
		*
		* @since  28.01.15
		* @author t
	]]
	function self:clearGroup( g )
		local p1, p2 = pcall(function()
			-- clear the contents of a group, but don't delete the group
			if(g ~= nil) then
				if(type(g.numChildren) == 'number') then
					for i=g.numChildren,1,-1 do
						display.remove( g[i] );
						g[i] = nil;		
					end
				end
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:clearGroup', p2);
		else
			return p2;
		end
	end	
			
	
	
	-- [[ **************  LISTENERS  ************** ]]
	
	
	--[[
		* Function: suspent and resume events listener
		*
		*
		* @since  28.01.15
		* @author t
	]]	
	function self.onSystemEvent( event )
		local p1, p2 = pcall(function()

			if (event.type == "applicationSuspend") then		
				self.onSuspend()
			elseif (event.type == "applicationResume") then
				self:removeLocalNotifications();
				-- USER OBJECT
				-- USER = UserClass.new({performWithDelay = 1000});

			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass.onSystemEvent', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: onSuspend listener
		*
		*
		* @since  28.01.15
		* @author t
	]]	
	function self.onSuspend()
		local p1, p2 = pcall(function()
		
			local stage = display.getCurrentStage()	
			self:clearGroup(stage);
			stage = nil;	
			
			collectgarbage();
			
			-- FOR MEMORY TEST
			if (performance) then	
				performance.group = nil
				performance.group = stat.createLayout(performance);
				timer.performWithDelay(1000,function() performance.indicator = true; end)
			end			
			
			if(USER and table.objectSize(USER) > 0)then
				USER:remove();
			end
			USER = nil;
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass.onSuspend', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: key listener
		*
		*
		* @since  28.01.15
		* @author t
	]]		
	function self.onKeyEvent( event )
		local p1, p2 = pcall(function()
			local phase = event.phase
			local keyName = event.keyName
			if keyName == 'back' then

				if phase == "down" then	

				elseif	phase == "up" then
					native.setKeyboardFocus( nil );
				end			

				return true;
			else
				return false;
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('ROOT.onKeyEvent', p2);
		else
			return p2;
		end
	end	
	
	
	--[[
		* Function: notification listener
		*
		*
		* @since  24.01.15
		* @author pcemma
	]]
	function self.onNotification( event )
		local p1, p2 = pcall(function()
			if event.type == "remoteRegistration" then
				self.deviceToken = event.token;
			elseif event.type == "remote" then
				native.showAlert( "LES", event.alert, { "OK" } );
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass.onNotification', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: отключает все нотификации
		*
		*
		* @since  02.02.15
		* @author pcemma
	]]
	function self:removeLocalNotifications()
		local p1, p2 = pcall(function()
			system.cancelNotification();
			native.setProperty( "applicationIconBadgeNumber", 0 );
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('RootClass:removeLocalNotifications', p2);
		else
			return p2;
		end
	end
	
		
	return self;
end

return RootClass;