print("CLASS UserClass IS CONNECTED");


local UserClass = {};

function UserClass.new(params)
	local self = GameDesignClass.new({});
	self.userId = 0;
	self.verifyHash = '';
	-- self.timers = {};
	-- self.timers.buildTimer = {};
	-- self.timers.marchesTimer = {};
	
	
	--[[
		*  Function: CLASS CONSTRUCTOR
		*
		* @params:	
		*	
		*
		*
		* @since  09.10.14
		* @author pcemma
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()
			local autoLoginData = self:getAutoLogin();
			print("\n-----------------\nautoLoginData\n");
			print_r(autoLoginData);
			print("\n-----------------\n");
			
			if autoLoginData.login or autoLoginData.password then						
				-- FOR AUTH TWO VARIOUS USERS
				if(_DEBUG and debugFirstUserData and debugSecondUserData)then
					if autoLoginData.login == debugFirstUserData.login then
						autoLoginData = debugSecondUserData;
						self:setAutoLogin(autoLoginData);
					elseif autoLoginData.login == debugSecondUserData.login then
						autoLoginData = debugFirstUserData;
						self:setAutoLogin(autoLoginData);
					end
				end
				
				self.login = autoLoginData.login;
				self.password = autoLoginData.password;
			end
			
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:__construct', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: create login interface
		*
		*
		*
		* @since  28.03.15
		* @author t
	]]
	function self:signin(data)
		local p1, p2 = pcall(function()
			print("LOGIN");			
			--**************  AUTH  **************
			
			-- local data = {login = self.login, password = self.password};
			-- data = self:addRegistrationDataToArray(data);				
			
			
			if(params.performWithDelay)then
				timer.performWithDelay(params.performWithDelay, function() networkManager:send({route = "auth", p = data }); end);
			else
				networkManager:send({route = 'test', p = data});
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:signin', p2);
		else
			return p2;
		end
	end

	
	--[[
		* Function: make user authorization. Fill User info with data
		*
		* @params:	array
		*	@data:			array, array of the users farm info (resources, resourcesGrowth, farm, units, marhes)
		*	@userId:		int, id of the current user
		*	@verifyHash:	str, secret string
		*
		*
		* @since  20.02.14
		* @author pcemma
	]]
	function self:authorization(params)
		local p1, p2 = pcall(function()
			-- SET USER DATA self = {farm[], marches,...}
			for i,v in pairs(params.data) do
				self[i] = v;
			end			
			
			-- get the number of the not completed empire quests
			self.noCompletedEmpireQuestsCount = self:getCountNoCompletedEmpireQuests();

			-- duplicate queues into  queuesRange
			self.queuesRange = table.clone(self.queuesRange);
	
			self.userId = params.userId or self.userId;
			self.userId = tostring(self.userId)
			self.verifyHash = params.verifyHash or self.verifyHash;
			
			-- start main timer
			self.globalTimerLastEventTime = os.time(os.date( '*t' ));
			self.globalTimer = timer.performWithDelay( 1000, self.timerLisnter, -1 );
			
			
			-- check if need write login to file
			if(params.autoLoginData and table.objectSize(params.autoLoginData) > 0)then
				self:setAutoLogin(params.autoLoginData);
				self.login = params.autoLoginData.login;
				self.password = params.autoLoginData.password;
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:authorization', p2);
		else
			return p2;
		end
	end
	
	
	
	--[[ OTHERS FUNCTIONS	]]
	
	
	--[[
		*  Function: that read auto config settings from file auto config
		*
		* @params:	
		*	
		*
		*
		* @since  21.01.15
		* @author pcemma
	]]
	function self:getAutoLogin()	
  		local p1, p2 = pcall(function()
			local autoLoginData = {};
			local path = system.pathForFile( "autoLogin", system.DocumentsDirectory );
			local fh, errStr = io.open( path, "r" );
			if fh then
				local content = fh:read( "*a" );
				if(content and content~= "")then
					autoLoginData = json.decode(content);
				end
			else
				fh = io.open( path, "w" );
			end
			io.close( fh );
			fh = nil;

			return autoLoginData;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:getAutoLogin', p2);
		else
			return p2;
		end	
	end	
	
	
	--[[
		* Записывает в файл измененые данные авторизации
		*
		* @params:	
		*
		*
		* @since  21.01.15
		* @author pcemma
	]]
	function self:setAutoLogin(data)
		local p1, p2 = pcall(function()
			local filePath = system.pathForFile( "autoLogin", system.DocumentsDirectory );
			local file, errStr = io.open( filePath, "w" );
			file:write(json.encode(data)); 
			file:close();
			file = nil;			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:setAutoLogin', p2);
		else
			return p2;
		end
	end	
	
	
	--[[
		* Записывает в присланый массив данные для регистрации
		*
		* @params:	
		*
		*
		*	@return: data, с новыми данными для регистрации
		*
		* @since  24.01.15
		* @author pcemma
	]]
	function self:addRegistrationDataToArray(data)
		local p1, p2 = pcall(function()
			data.uid = system.getInfo( "deviceID" );
			data.langLocale = system.getPreference("locale", "identifier");
			data.device = _DEVICE;
			data.deviceSystemVersion = system.getInfo("platformVersion"); -- platform
			data.deviceToken = Root.deviceToken;
			data.resolution = _W.."x".._H;
			data.clientVersion = _CLIENTVERSION;
			if(_DEVICE == "android")then
				data.deviceModel = system.getInfo("model");
			else
				data.deviceModel = system.getInfo("architectureInfo");
			end	
			
			return data;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('UserClass:data', p2);
		else
			return p2;
		end
	end	
	
	
	
	return self:__construct(params);
end

return UserClass;