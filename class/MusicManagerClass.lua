print("CLASS MusiicManagerClass IS CONNECTED");
local MusiicManagerClass = {};


function MusiicManagerClass.new(params)
	local self = {};
	self.loops = 0;
	self.marchAlarmFlag = false;
	
	--[[
		*  Function: конструктор класса
		*
		* @params:	
		*	
		*
		*
		* @since  28.01.15
		* @author pcemma
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()
			self.backgroundSoundsArray = {};
			self.sounds = {
				notification = audio.loadSound("sounds/sounds/notification.mp3"),
				buttonClick = audio.loadSound("sounds/sounds/buttonClick.mp3"),
				marchAlarm = audio.loadSound("sounds/sounds/marchAlarm.mp3"),
				getTrophy = audio.loadSound("sounds/sounds/getTrophy.mp3"),
				getMessage = audio.loadSound("sounds/sounds/getMessage.mp3")
			};
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:__construct', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: Удаления объекта 
		
		* @params:	
		
		
		
		* @since  08.02.14
		* @author pcemma, tooreckiy
	]]
	function self:remove()
		local p1, p2 = pcall(function()
			
			-- останавливаем таймер для фоновых звуков
			if(self.playBackgroundSoundTimer) then
				timer.cancel(self.playBackgroundSoundTimer);
			end
			
			-- останавливаем проигрывание фоновых звуков
			audio.stop();
			
			
			
			-- очищаем интерфейс
			if self then
				if self.interface then
					for i,v in pairs(self.interface) do
						display.remove(self.interface[i]);
						self.interface[i] = nil;
					end
				end
				-- очищаем все остальные данные
				for i,v in pairs(self) do
					display:remove(self[i]);
					self[i] = nil;
				end
			end
			self = nil;
			collectgarbage();
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:remove', p2);
		else
			return p2;
		end
	end

	
	--[[
		Function that play sound 
		params = {};
		params.file = slide_sound;         required parameter!!!
		params.chanel = 2;                 optional parameter
		params.volume = 0.5;               optional parameter
		params.loops = 0;                  optional parameter, for looping value must be set "-1"
		params.stop = false                optional parameter, to stop chanel value must be set "true"
		params.oncomplete = function() audio.play( bg_sound[2], { channel=1, loops = -1}) end;
	]]
	function self:playSound(params)	
		local p1, p2 = pcall(function()
			local sound;
			if params.chanel == nil then
				params.chanel = audio.findFreeChannel();
			end
			
			if audio.isChannelActive(params.chanel) then
				audio.stop( params.chanel );
				-- audio.dispose( laserSound );
			end
			
			params.loops = params.loops or self.loops;
			
			-- volume
			if params.volume ~= nil then
				params.volume = (params.volume * CONSTANT.settings.volume)/100;
			else
				params.volume = (1 * CONSTANT.settings.volume) / 100;
				
				if( CONSTANT.settings.effects == 0 ) then
					params.volume = 0;
				end				
			end
						
			if params.oncomplete ~= nil then
				sound = audio.play( params.file, { channel = params.chanel, onComplete = params.oncomplete} );
			else
				sound = audio.play( params.file, { channel = params.chanel, loops = params.loops} );
			end
			
			if(params.chanel and params.volume) then
				audio.setVolume( params.volume, { channel = params.chanel } );
			else
				audio.setVolume( params.volume, { channel = sound } );
			end
			
			return sound;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:playSound', p2);
		else
			return p2;
		end
	end

		
	--[[
		*  Function: Проигрывает фон бекграунда.
		*
		*	
		*
		*
		* @since  28.01.15
		* @author pcemma
	]]
	function self:playBackGround()
		local p1, p2 = pcall(function()
			local differentSounds = {};
			local arrTemp = {};
			local volume = 0.25;
			
			if CONSTANT.settings.music == 0 then
				volume = 0;	
			end
			
			for i = 1, 3 do
				local sound = math.random(1, 7);
				while table.inArray(arrTemp , sound) do
					sound = math.random(1, 7);
				end

				table.insert(self.backgroundSoundsArray, self:playSound({file = audio.loadStream("sounds/background/"..sound..".mp3"), volume = volume}));
				arrTemp[i] = sound;
			end
			
			self.playBackgroundSoundTimer = timer.performWithDelay(39000, function() self:playBackGround(); end);
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:playBackGround', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: change BackGround Volume
		*
		*	
		*
		*
		* @since  04.02.15
		* @author t
	]]
	function self:changeBackGroundVolume()
		local p1, p2 = pcall(function()
			local volume = 0.25;
			
			if( CONSTANT.settings.music == 0 ) then
				volume = 0;
			end
			
			for i,v in pairs(self.backgroundSoundsArray) do
				audio.setVolume( volume, { channel = self.backgroundSoundsArray[i] } );														
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:changeBackGroundVolume', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: Стартует систему аларма маршей
		*
		*	
		*
		*
		* @since  29.01.15
		* @author pcemma
	]]
	function self:startMarchAlarm()
		local p1, p2 = pcall(function()
			if(not self.marchAlarmFlag)then
				self.marchAlarmFlag = true;
				self:playMarchAlarm();
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:startMarchAlarm', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: Проигрывает систему аларма маршей
		*
		*	
		*
		*
		* @since  29.01.15
		* @author pcemma
	]]
	function self:playMarchAlarm()
		local p1, p2 = pcall(function()
			local isMarch = false;
			-- stop timer
			if(self.playMarcheAlarmTimer)then
				timer.cancel(self.playMarcheAlarmTimer);
			end
			
			for i,v in pairs(USER.marches) do
				if(table.inArray({1,5,6}, tonumber(v.marchType)) and v.oponentId == USER.userId and not v.retrieveMarchFlag)then
					isMarch = true;
					break;
				end
			end
			
			if(isMarch)then
				self:playSound({file = self.sounds.marchAlarm});
				self.playMarcheAlarmTimer = timer.performWithDelay(10000, function() self:playMarchAlarm(); end);
			else
				self.marchAlarmFlag = false;
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('MusiicManagerClass:playMarchAlarm', p2);
		else
			return p2;
		end
	end
	
	
	
	return self:__construct(params);
end

return MusiicManagerClass;