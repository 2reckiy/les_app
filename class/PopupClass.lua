print("CLASS POPUP IS CONNECTED");

local PopupClass = {};

function PopupClass.new(params)
	local self =  GameDesignClass.new({});
	self.mainTitle = ""; -- info string in popup
	self.question = ""; --midle text in popup
	self.width = _W * 0.906;
	self.bgAlpha = 0.4;
	self.bgColor = {1,1,1};
	
	
	self.borderScale = _W * 0.017;
	self.borderIndent = _W * 0.047;
	self.bottomIndent = _W * 0.029;
	self.interlineIndent = _W * 0.0375;
	self.buttonIndent = _W * 0.075 + self.borderScale;

	self.closeX = _W * 0.06;
	self.closeY = _W * 0.051;

	--[[
		* Function: Создает попап
		*
		*	@params:	array
		*		@buttons:	array, array of the buttons. key of the element is a button image. value if function
		*		@mainTitle:		str, the main title of the popup
		*		@height:		int, height of the popup,
		*		@width:			int, width of the popup,
		*
		* @since  10.10.14
		* @author t
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()
			-- check params
			self.mainTitle = params.mainTitle or self.mainTitle;
			self.question = params.question or self.question;
			self.closeFunc = params.closeFunc or function() end;
			
			self.bgAlpha = params.bgAlpha or self.bgAlpha;
			self.bgColor = params.bgColor or self.bgColor;
			self.buttons = params.buttons or {};
			self.stencil = params.stencil or "";
			self.data = params.data or {};
			
			
			-- INTERFACE
			self.interface = {};	
			self.interface.buttons = {};
			self.interface.group = display.newGroup();
			self.interface.group.x = _W / 2;
			self.interface.group.y = _H / 2;
			
			if globalGroup and globalGroup[3] and globalGroup[3].alpha ~= nil then
				globalGroup[3]:insert(self.interface.group);
			end
			
			-- create background not touch object
			self.interface.bgNotTouch = self:createBgNotTouch({
																group = self.interface.group,
																alpha = self.bgAlpha, 
																startX = 0, 
																startY = 0,
																width = _W,
																height = _H,
																color = self.bgColor,
																x = 0,
																y = 0
															});
			-- CREATE BG
			self.interface.poupGroup = display.newGroup();
			self.interface.poupGroup.x = - _W*0.5;
			self.interface.poupGroup.y = - _H*0.5;
			self.interface.group:insert(self.interface.poupGroup);
			
			-- CREATE BORDER
			
			-- LEFT UP
			self.interface.left_up_border = self:createImage({	
																file = "images/popup/bg/left_up.png",
																group = self.interface.poupGroup,
																scale = self.borderScale, 
																scaleType = 'x',
																anchor_x = 0,
																anchor_y = 0,
																y = 0, 
																x = self.borderIndent
															});
			-- UP
			self.interface.up_border = self:createImage({	
																file = "images/popup/bg/up.png",
																group = self.interface.poupGroup,
																scale_y = self.borderScale, 
																scale_x = self.width-self.borderScale*2,
																anchor_x = 0,
																anchor_y = 0,
																y = 0, 
																x = self.interface.left_up_border.x + self.interface.left_up_border.contentWidth
															});
			-- RIGHT UP
			self.interface.right_up_border = self:createImage({	
																file = "images/popup/bg/right_up.png",
																group = self.interface.poupGroup,
																scale = self.borderScale, 
																scaleType = 'x',
																anchor_x = 0,
																anchor_y = 0,
																y = 0, 
																x = self.interface.up_border.x + self.interface.up_border.contentWidth
															});

			-- LEFT
			self.interface.left_border = self:createImage({	
																file = "images/popup/bg/left.png",
																group = self.interface.poupGroup,																
																scale_x = self.borderScale,
																anchor_x = 0,
																anchor_y = 0,
																y = self.borderScale, 
																x = self.borderIndent
															});	

			-- CENTER
			self.interface.center_border = self:createImage({	
																file = "images/popup/bg/center.png",
																group = self.interface.poupGroup,																
																scale_x = self.width-self.borderScale*2,
																anchor_x = 0,
																anchor_y = 0,
																y = self.borderScale, 
																x = self.interface.left_border.x + self.interface.left_border.contentWidth
															});															

			-- RIGHT
			self.interface.right_border = self:createImage({	
																file = "images/popup/bg/right.png",
																group = self.interface.poupGroup,																
																scale_x = self.borderScale,
																anchor_x = 0,
																anchor_y = 0,
																y = self.borderScale, 
																x = self.interface.center_border.x + self.interface.center_border.contentWidth
															});															


			-- LEFT DOWN
			self.interface.left_down_border = self:createImage({	
																file = "images/popup/bg/left_down.png",
																group = self.interface.poupGroup,
																scale = self.borderScale, 
																scaleType = 'x',
																anchor_x = 0,
																anchor_y = 0,
																y = self.interface.center_border.y + self.interface.center_border.contentHeight, 
																x = self.borderIndent
															});
			-- DOWN
			self.interface.down_border = self:createImage({	
																file = "images/popup/bg/down.png",
																group = self.interface.poupGroup,
																scale_y = self.borderScale, 
																scale_x = self.width-self.borderScale*2,
																anchor_x = 0,
																anchor_y = 0,
																y = self.interface.left_down_border.y, 
																x = self.interface.left_down_border.x + self.interface.left_down_border.contentWidth
															});
			-- RIGHT DOWN
			self.interface.right_down_border = self:createImage({	
																file = "images/popup/bg/right_down.png",
																group = self.interface.poupGroup,
																scale = self.borderScale, 
																scaleType = 'x',
																anchor_x = 0,
																anchor_y = 0,
																y = self.interface.left_down_border.y, 
																x = self.interface.down_border.x + self.interface.down_border.contentWidth
															});															
			
			-- CREATE CONTENT GROUP
			self.interface.body = display.newGroup();
			self.interface.body.enterFrameListener = function() end;
			
		
			
			self.interface.poupGroup:insert(self.interface.body);
			self.interface.body.x = self.borderIndent;
			
			-- FILL CONTENT
			self:fillBody();
			
			-- CHANGE POPUP HEIGHT BY CONTENT
			self:changePopupHeight();
			
			return self;
			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass:__construct', p2);
		else
			return p2;
		end
	end


	--[[
		* Function: FILL CONTENT
		*
		*
		* @since  10.10.14
		* @author t
	]]
	function self:fillBody()
		local p1, p2 = pcall(function()
		
			self.contentY = 0;
			
			-- TITLE
			if self.mainTitle and self.mainTitle ~= "" then
				self.interface.mainTitle = self:createTitle({
																txt = self.mainTitle,
																txt_size = 29*CONSTANT.textScale,
																color = CONSTANT.lightGreyTextColor,
																group = self.interface.body,
																anchor_x = .5,
																anchor_y = 0,
																x = self.width * 0.5,
																width = self.width - (self.closeX + _W*0.109375*0.5)*2,
																align = "center",
																-- width = self.width * 0.8,
																-- align = "center",
																y = _W * 0.03
															});
												
				self.contentY = self.interface.mainTitle.y + self.interface.mainTitle.contentHeight + self.interlineIndent;
				
				-- LINE
				self.interface.line = self:createRect({	
														group = self.interface.body,														
														width = self.width - (self.closeX + _W*0.109375*0.5)*2,
														height = 2,
														anchor_y = 0,
														anchor_x = 0.5,
														y = self.interface.mainTitle.y + self.interface.mainTitle.contentHeight, 
														x = self.width * 0.5,
														color= {0.439, 0.419, 0.4}
													});
			end
			
			-- CLOSE BUTTON
			self.interface.closeButton = self:createButton({	onEvent = function(event) 
																				if event.phase == "begin" then
																				elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																					-- проигрываем звук
																					musicManager:playSound({file = musicManager.sounds.buttonClick});
																					self.closeFunc();
																					self:close();
																					return true;
																				end
																			end, 
																defaultFile = "images/popup/close.png", 
																overFile = "images/popup/close_pressed.png", 
																title = "",
																group = self.interface.body, 
																-- scale = _W*0.109375, 
																scale = _W*0.121, 
																scaleType = "x",
																anchor_x = .5,
																anchor_y = .5,
																x = self.width - self.closeX,
																y = self.closeY,
															});	
			if self.stencil ~= "" then
			-- UNIQUE POPUP
				self['createPopupStencil_'..self.stencil]();
			else
			-- STANDART POPUP
			
				-- TEXT \ QUESTION
				if self.question ~= "" then
					self.interface.question = self:createTitle({
																	txt = self.question,
																	txt_size = 20*CONSTANT.textScale,
																	color = CONSTANT.lightGreyTextColor,
																	group = self.interface.body,
																	width = (self.width-self.borderScale*2),
																	align = 'center',
																	anchor_x = 0,
																	anchor_y = 0,
																	x = self.borderScale,
																	y = self.contentY
																});
					self.contentY = self.contentY + self.interface.question.contentHeight + self.interlineIndent;
				end
				
				-- BUTTONS			
				local buttonScale = (self.width-self.borderScale*2)/table.objectSize(self.buttons);
				if buttonScale > CONSTANT.smallButtonScale then buttonScale = CONSTANT.smallButtonScale; end
				
				local buttonInterval = 0;
				if buttonScale*table.objectSize(self.buttons) < self.width-self.borderScale*2 then
					buttonInterval = (self.width-self.borderScale*2 - buttonScale*table.objectSize(self.buttons)) / (table.objectSize(self.buttons)+1)
				end
				
				self.interface.buttonsGroup = display.newGroup();
				self.interface.body:insert(self.interface.buttonsGroup);
				
				
				for i,v in pairs(self.buttons) do
					
					self.interface.buttons[i] = self:createButton({	onEvent = function(event) 
																					if event.phase == "begin" then
																					elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																						-- проигрываем звук
																						if(musicManager and table.objectSize(musicManager) > 0)then
																							musicManager:playSound({file = musicManager.sounds.buttonClick});
																						end
																						
																						-- execution of the function
																						self.buttons[i].func();
																						self:close();																					
																						return true;
																					end
																				end, 
																	sheet = CONSTANT.buttonsImageSheet1,
																	defaultFrame = 15,
																	overFrame = 13,
																	title = self.buttons[i].title or "Default",
																	fontSize = 25,
																	group = self.interface.buttonsGroup, 
																	scale = buttonScale, 
																	scaleType = "x",
																	anchor_x = 0,
																	anchor_y = 0,
																	x = self.borderScale + buttonInterval*tonumber(i) + buttonScale*tonumber(i-1),
																	y = 0,
																	});

				end
				
				self.interface.buttonsGroup.y = self.contentY;
			end
			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass:fillBody', p2);
		else
			return p2;
		end
	end
	

	--[[
		* Function: CHANGE POPUP HEIGHT BY CONTENT
		*
		*
		* @since  10.10.14
		* @author t
	]]
	function self:changePopupHeight()
		local p1, p2 = pcall(function()
			if self.interface.buttonsGroup and self.interface.buttonsGroup.alpha ~= nil then
				self.interface.buttonsGroup.y = self.contentY;
			end
			
			local height = self.interface.body.contentHeight + self.bottomIndent;
			
			self.interface.left_border.yScale = height;
			self.interface.center_border.yScale = height;
			self.interface.right_border.yScale = height;
			
			self.interface.left_down_border.y = self.interface.left_border.y + self.interface.left_border.contentHeight;
			self.interface.down_border.y = self.interface.left_down_border.y;
			self.interface.right_down_border.y = self.interface.left_down_border.y;
			
			self.interface.poupGroup.y = - self.interface.poupGroup.contentHeight/2
			
			-- FOR TUTORIAL
			if self.interface.tutorialHeroImage then
				self.interface.tutorialHeroImage.y = self.interface.left_down_border.y +  self.interface.left_down_border.contentHeight - self.interface.tutorialHeroImage.contentHeight;
			end	
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass:changePopupHeight', p2);
		else
			return p2;
		end
	end	

	
	--[[
		* Function: CLOSE POPUP
		*
		*
		* @since  05.11.14
		* @author t
	]]
	function self:close()
		local p1, p2 = pcall(function()
			-- CANCEL TIMER
			if(self.timer)then
				timer.cancel(self.timer);
			end
			
			Runtime:removeEventListener("enterFrame", self.enterFrame);
			
			self:remove();
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass:close', p2);
		else
			return p2;
		end
	end	
	
	
	
	--[[          HELP FUNCTIONS          ]]--	

	

	--[[
		* Function: enter frame для попапа
		*
		*	
		*
		* @since  22.12.14
		* @author pcemma
	]]
	function self.enterFrame(event)
		local p1, p2 = pcall(function()
			if(self and self.interface and self.interface.body and self.interface.body.alpha ~= nil)then
				self.interface.body.enterFrameListener();
			else
				Runtime:removeEventListener("enterFrame", self.enterFrame);
			end
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass.enterFrame', p2);
		else
			return p2;
		end
	end	
	

	
	--[[          POPUP STENCILS          ]]--
	
	

	--[[
		* Function: CREATE UNIQUE POPUP by STENCIL: "ci" - castle info
		*
		*	
		*
		* @since  10.10.14
		* @author t
	]]
	function self.createPopupStencil_ci()
		local p1, p2 = pcall(function()

			-- PLUS BUTTON
			self.interface.plusButton = self:createButton({	onEvent = function(event) 
																				if event.phase == "begin" then
																				elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																					-- проигрываем звук
																					musicManager:playSound({file = musicManager.sounds.buttonClick});
																					if(USER.mapNotes and table.objectSize(USER.mapNotes) < tonumber(CONSTANT.usersMapNotesCount))then
																						networkManager:send({route = "addMapNotes", p = {x = self.data.x, y = self.data.y, title = self.mainTitle } });
																						self.interface.plusButton:enable(false);
																					else
																						local popup = PopupClass.new({mainTitle = T:translate("noFreeMapsNotesPopupTitle", {}),question =  T:translate("noFreeMapsNotesPopupDesc", {})});
																					end
																					return true;
																				end
																			end, 
																defaultFile = "images/popup/plusButton.png", 
																overFile = "images/popup/plusButton_pressed.png", 
																title = "",
																group = self.interface.body, 
																scale = _W*0.072, 
																scaleType = "x",
																anchor_x = .5,
																anchor_y = .5,
																x = _W*0.06875, 
																y = _W*0.046875, 
															});
			
			-- if(USER.mapNotes and table.objectSize(USER.mapNotes) >= tonumber(CONSTANT.usersMapNotesCount))then
				-- self.interface.plusButton:enable(false);
			-- end

			-- GODS HELP ICON
			if self.data.info.godsHelpFlag then
				
				-- нам надо знать оригинальную ширину тайтла главного.
				local tempTitle = self:createTitle({
													txt = self.mainTitle,
													txt_size = 29*CONSTANT.textScale,
													color = CONSTANT.lightGreyTextColor,
													group = self.interface.body,
													anchor_x = .5,
													anchor_y = 0,
													x = self.width * 0.5,
													-- width = self.width * 0.8,
													-- align = "center",
													y = _W * 0.03
												});
				local tempTitleWidth = tempTitle.contentWidth;
				display.remove(tempTitle);
				tempTitle = nil;
				
				self.interface.godsHelpIcon = self:createImage({	
																file = "images/map/godsPower.png",
																group = self.interface.body,
																scale = _W * 0.054, 
																scaleType = "x", 
																anchor_x = 1,
																anchor_y = .5,
																y = self.interface.mainTitle.y + self.interface.mainTitle.contentHeight * 0.5,
																x = self.width * 0.5 - tempTitleWidth * 0.5
															});			
			end
			-- AVATAR BG
			self.interface.avatarBg = self:createImage({	
																file = "images/popup/avatars/avatarBg.png",
																group = self.interface.body,
																scale = _W*0.215625, 
																scaleType = 'x',
																anchor_x = 0.5,
																anchor_y = 0.5,
																y = _W*0.2296875, 
																x = _W*0.14375
															});
															
			-- AVATAR
			self.interface.avatar = self:createImage({	
														file = "images/popup/avatars/"..self.data.info.heroId..".png",
														group = self.interface.body,
														scale = _W*0.215625, 
														scaleType = 'x',
														anchor_x = 0.5,
														anchor_y = 0.5,
														y = _W*0.2296875, 
														x = _W*0.14375
													});
			local h = _W*0.13;
			-- POWER
			self.interface.power = self:createImage({	
														file = "images/headerMenu/power.png",
														group = self.interface.body,
														scale = _W*0.05625, 
														scaleType = 'y',
														anchor_x = 0.5,
														anchor_y = 0,
														y = h, 
														x = self.interface.avatar.x + self.interface.avatar.contentWidth/2 + _W*0.011*2 + _W*0.05625*0.5,
													});
			-- POWER VALUE
			self.interface.powerValue = self:createTitle({
															txt = self.data.info.power,
															txt_size = 25*CONSTANT.textScale,
															color = CONSTANT.lightGreyTextColor,
															group = self.interface.body,
															anchor_x = 0,
															anchor_y = 0.5,
															x = _W*0.34375,
															y = h + _W*0.05625 * 0.5
														});												
													
			h = h + self.interface.power.contentHeight;											
			-- KILL
			self.interface.kill = self:createImage({	
														file = "images/progressBar/types/2.png",
														group = self.interface.body,
														scale = _W*0.05625, 
														scaleType = 'y',
														anchor_x = 0.5,
														anchor_y = 0,
														y = h, 
														x = self.interface.avatar.x + self.interface.avatar.contentWidth/2 + _W*0.011*2 + _W*0.05625*0.5,
													});	
			-- KILL VALUE
			self.interface.killValue = self:createTitle({
															txt = self.data.info.kill,
															txt_size = 25*CONSTANT.textScale,
															color = CONSTANT.lightGreyTextColor,
															group = self.interface.body,
															anchor_x = 0,
															anchor_y = 0.5,
															x = _W*0.34375,
															y = h + _W*0.05625 * 0.5
														});													
			h = h + self.interface.kill.contentHeight;
			
			if self.data.info.allianceName then
				-- ALLIANCE
				self.interface.alliance = self:createImage({	
															file = "images/alliance/allianceHouse/alliance.png",
															group = self.interface.body,
															scale = _W*0.05625, 
															scaleType = 'y',
															anchor_x = 0.5,
															anchor_y = 0,
															y = h, 
															x = self.interface.avatar.x + self.interface.avatar.contentWidth/2 + _W*0.011*2 + _W*0.05625*0.5,
														});		
				-- ALLIANCE VALUE
				self.interface.allianceValue = self:createTitle({
																-- txt = "["..map.areasInfo[self.data.areaId][self.data.x.."."..self.data.y].tag.."]"..self.data.info.allianceName,
																txt = "["..self.data.info.allianceTag.."]"..self.data.info.allianceName,
																txt_size = 25*CONSTANT.textScale,
																color = CONSTANT.lightGreyTextColor,
																group = self.interface.body,
															anchor_x = 0,
															anchor_y = 0.5,
															x = _W*0.34375,
															y = h + _W*0.05625 * 0.5
															});														
				h = h + self.interface.alliance.contentHeight;			
			end
			
			-- STATE
			self.interface.state = self:createImage({	
														file = "images/headerMenu/statesListButton.png",
														group = self.interface.body,
														scale = _W*0.05625, 
														scaleType = 'y',
														anchor_x = 0.5,
														anchor_y = 0,
														y = h, 
														x = self.interface.avatar.x + self.interface.avatar.contentWidth/2 + _W*0.011*2 + _W*0.05625*0.5,
													});
			-- STATE VALUE
			self.interface.stateValue = self:createTitle({
															txt = DATA.states[tostring(self.data.k)][T.lang.."_name"],
															txt_size = 25*CONSTANT.textScale,
															color = CONSTANT.lightGreyTextColor,
															group = self.interface.body,
															anchor_x = 0,
															anchor_y = 0.5,
															x = _W*0.34375,
															y = h + _W*0.05625 * 0.5
														});													
			h = h + self.interface.state.contentHeight;

			-- COORDINATES
			self.interface.coordinates = self:createTitle({
															-- txt = T:translate('kingdomCoordinats:').."X: "..self.data.x..", Y: "..self.data.y,
															txt = "X: "..self.data.x..", Y: "..self.data.y,
															txt_size = 18*CONSTANT.textScale,
															color = CONSTANT.lightGreyTextColor,
															group = self.interface.body,
															anchor_x = 0.5,
															anchor_y = 0,
															x = self.interface.avatar.x,
															y = self.interface.avatar.y + self.interface.avatar.contentHeight/2
														});
			
			self.contentY = self.contentY + self.interface.avatar.contentHeight + self.interface.coordinates.contentHeight;
					
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('PopupClass:createPopupStencil_ci', p2);
		else
			return p2;
		end
	end	

	
	return self:__construct(params);
end

return PopupClass;
