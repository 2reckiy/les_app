print("CLASS NetworkClass IS CONNECTED");


local NetworkClass = {};

function NetworkClass.new(params)

	local self = {};
	
	-- в строке хранится предыдущая ошибка на клиенте
	self.lastClientError = "";
	
	-- Array of execute functions
	self.responses = {};

	self.headers = {};
	-- self.headers["Accept-Encoding"] = "gzip";
	-- NetworkClass.headers["Content-Type"] = "application/x-www-form-urlencoded";
	-- NetworkClass.headers["Accept-Language"] = "en-US";


	--[[
		* Function: конструктор класса
		*
		* @params:	
		*	
		*
		*
		* @since  26.03.14
		* @author pcemma
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()
			self.networkErrorCount = 0;
			
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('NetworkClass:__construct', p2);
		else
			return p2;
		end
	end


	--[[
		* Function: CREATE HELLO SCREEN
		*
		*
		* @since  02.10.14
		* @author tooreckiy
	]]
	function self:networkErrorListener()
		local p1, p2 = pcall(function()
		
			if self.networkErrorCount == 3 then		
				if not self.networkErrorListenerFlag then 
					self.networkErrorListenerFlag = true;
					PopupClass.new({	mainTitle = T:translate("networkError", {}), 
										buttons = {
													['1'] = {
																func = 	function()
																			Root.onSuspend();
																			Root:removeLocalNotifications();
																			self.networkErrorListenerFlag = nil;
																			self.networkErrorCount = 0;
																			--USER OBJECT
																			USER = UserClass.new({performWithDelay = 1000});
																		end, 
																title = T:translate("reconnect", {})
															} 
										},
										closeFunc = function()
														self.networkErrorListenerFlag = nil;
														self.networkErrorCount = 0;
													end
									});
				end	
			else
				self.networkErrorCount = self.networkErrorCount + 1;
			end																										
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('NetworkClass:networkErrorListener', p2);
		else
			return p2;
		end
	end	
	
	
	--[[
		* Function that create an image
		* @params:	array, array of params
		
		
		* @since  10.02.14
		* @author pcemma, tooreckiy
	]]
	self.networkListener = function( event )
		local p1, p2 = pcall(function()

			if ( event.isError ) then
				print( "Network error!" );
				print_r(event);
				self:send_client_error_to_server('NetworkClass:networkListener.NetworkError', json.encode(event));
				if startScreen then
					self.networkErrorCount = 3;
				end
				self:networkErrorListener();
				-- network.cancel( event.requestId );
			else
				print('\n-------------------\nnetworkListener');
				
				self.networkErrorCount = 0;
				
				-- if startScreen then
					-- event.responseHeaders.len = event.responseHeaders.len or 1;
					-- startScreen:progress({percent = math.round(math.abs(event.bytesTransferred/event.responseHeaders.len*100 - startScreen.interface.progress.currentValue))});					
				-- end
				
				if(event.response)then
					local response = json.decode(event.response);
					
					if ( type(response) == "table" ) then						
						print_r(response)
						if not response.errorCode then
							if self.responses[response.f] then
								timer.performWithDelay(1, function() self.responses[response.f](response.p); end)
							end
						else
							-- ERROR
							PopupClass.new({ mainTitle = T:translate("error", {}), question =  response.error });
						end
					else
						
					end
				end
				
				print('\n-------------------\n');
			end
		end);
		if(p1 == false) then
			self:send_client_error_to_server('NetworkClass:networkListener', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that create an image
		* @params:	array, array of params
		*	[
		*		@requestType: 	str, type of the request ("POST", "GET")
		*		@route:			str, what route we need to call
		*		@p:				array, array of params for function
		*	]
		* @since  08.02.14
		* @author pcemma, tooreckiy
	]]
	function self:send(params)
		local p1, p2 = pcall(function()
			L:loading(true);
			
			if(not params.requestType)then
				params.requestType = "POST";
			end
			
			if(not params.route) then
				params.route = "";
			end
			
			
			-- for verify user
			if(USER and table.objectSize(USER) ~= 0)then
				params.p.userId = USER.userId;
				params.p.verifyHash = USER.verifyHash;
			end
			
			local p = {body = "params="..json.encode(params.p), headers = self.headers};

			if params.route == 'auth' then
				p.progress = "download";
			end
			
			network.request( _IP..params.route..".php", params.requestType, self.networkListener, p);

		end);
		if(p1 == false) then
			self:send_client_error_to_server('NetworkClass:send', p2);
		else
			return p2;
		end
	end


	--[[
		* Function that send error log to server
		*
		* @func_name:	str, the name of the function in which ERROR is
		* @p2:			str, the error log
		*
		* @since  10.02.14
		* @author pcemma, tooreckiy
	]]
	function self:send_client_error_to_server(func_name, p2)
		local p1, p2 = pcall(function()
			print ("Client ERROR!!!");
			if(self.lastClientError ~= p2)then
				self.lastClientError = p2;
				print_r(p2);
				-- self:send({route = 'makeClientsErrorLogs', p = {error = p2, functionName = func_name, clientVersion = _CLIENTVERSION or ""}});
			end
		end);
		if(p1 == false) then
			self:send_client_error_to_server('NetworkClass:send_client_error_to_server', p2);
		else
			return p2;
		end
	end
		

		
	--[[ **************  SOCKET  ************** ]]

	--[[
		* function to get server response
		
		*
		* @since  26.03.14
		* @author pcemma, tooreckiy
	]]
	function self.sockget(event)
		local p1, p2 = pcall(function()
			local r = nil;
			local tmpr, tmpe = nil;
			sockget_flag = true;
			tmpr, tmpe = client:receive(6);

			if tmpr ~= nil then
				-- print(tmpr);
				tmpbytes=tonumber(tmpr);
				if tmpbytes > 1000 then
					client:settimeout(10);
				end
				r, e = client:receive(tmpbytes);
				client:settimeout(0);

				if(r ~= nil) then
					-- print(r);
					local arr = json.decode(r);
					if arr ~= nil then
						
						-- IF P==0 THEN NEED TO CHECK UPDATES QUEUE
						if arr.p == 0 then
							networkManager:send({ route = "checkUpdatesQueueOnServer", p = {} });
						end
					else
						sockget_flag = false;
					end
				end
			end	
			client:settimeout(0);
			sockget_timer = timer.performWithDelay (100, self.sockget);
		end);
		if(p1 == false) then
			self:send_client_error_to_server('NetworkClass.sockget', p2);
		else
			return p2;
		end
	end


	--[[
		* function to set socket connection to server
		
		*
		* @since  26.03.14
		* @author pcemma, tooreckiy
	]]
	function self:socketConnection()
		local p1, p2 = pcall(function()
			client = socket.tcp();
			local str,err = client:connect(_SOCKETIP, _SOCKETPORTLISTTEN);
			if str == nil then
				ip = nil;
			else	
				ip, port = client:getsockname();
				
				local params = {}
				params.userId = USER.userId;
				params.verifyHash = USER.verifyHash;
				
				local string_params = json.encode(params);
				local bytes_count = return_bytes(string_params);

				local err, msg = client:send(bytes_count..string_params, 1);

				if msg == "closed" then
					ip = nil;
				end		

				
				-- RUN SOCKET LISTENER
				timer.performWithDelay (100, self.sockget);
			end
		end);
		if(p1 == false) then
			self:send_client_error_to_server('NetworkClass:socketConnection', p2);
		else
			return p2;
		end
	end
	
	
	
	return self:__construct(params);
end

return NetworkClass;
