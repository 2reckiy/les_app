print("CLASS StartScreenClass IS CONNECTED");


local StartScreenClass = {};

function StartScreenClass.new()
	local self = GameDesignClass.new({});
	
	--[[
		* Function: конструктор стартвой сцены
		*
		* @params:	
		*	
		*
		*
		* @since  04.11.14
		* @author pcemma
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()
			self.transitions = {};
			self.loginPanelHeight = _H * .5;
			
			self.interface = {};
			self.interface.group = display.newGroup();
			
			
		
			self.interface.bg = self:createRect({
													group = self.interface.group,
													width = _W,
													height = _H,													
													anchor_x = 0.5,
													anchor_y = 0.5,
													y = _H/2,
													x = _W/2,
													color ={1,1,1}
												});
			if(self.interface.bg.contentWidth < _W )then
				local k = _W / self.interface.bg.contentWidth;
				self.interface.bg.xScale = self.interface.bg.xScale * k;
				self.interface.bg.yScale = self.interface.bg.yScale * k;
			end
			
			-- логотип названия
			self.interface.logo = self:createImage({
													file = "images/logo.png",
													group = self.interface.group,
													scale = _W * 0.2, 
													scaleType = "x",
													anchor_x = 0.5,
													anchor_y = 0.5,
													y = _H/2,
													x = _W/2
												});
			self.interface.logo.y = _W * 0.041 + self.interface.logo.contentHeight * 0.5;
			
			print("USER.login",USER.login)
			if USER.login then
				print('createEnteringInterface')
				self:createEnteringInterface();
			else
				print('createLoginInterface')
				self:createLoginInterface();
			end		
			
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:__construct', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: create Entering Interface
		*
		*
		* @since  28.03.15
		* @author t
	]]
	function self:createEnteringInterface()
		local p1, p2 = pcall(function()		
			-- bg for entering
			self.interface.enteringBg = self:createRect({
															group = self.interface.group,
															width = _W,
															height = _H,
															anchor_x = .5,
															anchor_y = .5,
															x = _W*.5,
															y = _H*.5,
															color = {1,1,1},
															alpha = 0.01
														});
			-- entering bg listener											
			self.interface.enteringBg:addEventListener('tap', 	function()
																	local p1, p2 = pcall(function()
																		if self.transitions.enteringTtileTransition then
																			transition.cancel(self.transitions.enteringTtileTransition);
																		end
																		self.interface.enteringTtile.isVisible = false;
																	end)
																	if(p1 == false) then
																		networkManager:send_client_error_to_server('StartScreenClass:enteringBgListener', p2);
																	else
																		return p2;
																	end
																end)

			-- entering blink title
			self.interface.enteringTtile = self:createTitle({
																txt = T:translate("enteringTitle"),
																txt_size = 29*CONSTANT.textScale,
																color = CONSTANT.lightGreyTextColor,
																group = self.interface.group,
																anchor_x = .5,
																anchor_y = 0,
																x = _W * 0.5,
																y = _H * .9
															});																
			self.transitions.enteringTtileTransition = transition.blink( self.interface.enteringTtile, { time = 1500 } );

			-- login button
			self.interface.loginButton = self:createButton({	onEvent = 	function(event)
																				local p1, p2 = pcall(function()
																					if event.phase == "begin" then
																					
																					elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																						
																						
																						return true;
																					end
																					
																				end)
																				if(p1 == false) then
																					networkManager:send_client_error_to_server('StartScreenClass:loginButtonListener', p2);
																				else
																					return p2;
																				end					
																			end,
																group = self.interface.group,
																
																sheet = CONSTANT.buttonsImageSheet1,
																defaultFrame = 15,
																overFrame = 13,																
																
																title = "Change account",
																fontSize = 25 * CONSTANT.textScale,					
																
																scale = CONSTANT.longButtonScale * 0.3, 
																scaleType = "x",
																anchor_x = .5,
																anchor_y = 1,
																x = _W * 5,
																y = _H * .99,
															});				
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:createEnteringInterface', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: create Login Interface
		*
		*
		* @since  28.03.15
		* @author t
	]]
	function self:createLoginInterface()
		local p1, p2 = pcall(function()
			
			-- LOGIN GROUP
			self.interface.loginGroup = display.newGroup();
			self.interface.group:insert(self.interface.loginGroup);
			self.interface.loginGroup.y = 0;
			
			-- LOGIN BG
			self.interface.loginBg = self:createRect({
														group = self.interface.loginGroup,
														width = _W,
														height = _H,
														anchor_x = 0,
														anchor_y = 1,
														x = 0,
														y = 0,
														color = {0,0.5,1},
														alpha = 1
													});

			
			-- login button group
			self.interface.loginButton = display.newGroup();
			self.interface.loginGroup:insert(self.interface.loginButton);

			-- login button bg
			self.interface.loginButton.bg = self:createRect({
																group = self.interface.loginButton,
																width = _W,
																height = _H * 0.07,
																anchor_x = 0,
																anchor_y = 0,
																x = 0,
																y = 0,
																color = {0,0,0},
																alpha = 0.01
															});
			self.interface.loginButton.bg:addEventListener("touch", function(event)
																		local p1, p2 = pcall(function()
																			if event.phase == "begin" then
																			
																			elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																			
																				self:openHideLoginPanel();
																				
																				return true;
																			end
																			
																		end)
																		if(p1 == false) then
																			networkManager:send_client_error_to_server('StartScreenClass:createLoginInterface loginButtonListener', p2);
																		else
																			return p2;
																		end				
																	end);
																	
			-- login button title
			self.interface.loginButton.title = self:createTitle({
																txt = T:translate("login"),
																txt_size = 30*CONSTANT.textScale,
																color = CONSTANT.greyTextColor,
																group = self.interface.loginButton,
																anchor_x = 1,
																anchor_y = .5,
																x = _W * 0.95,
																y = _H * 0.07 * 0.5
															});													
			
			-- userName Input
			self.interface.loginButton.userNameInput = self:createInput({
																			group = self.interface.loginButton,
																			width = _W * .8,
																			height = _H * .1,
																			anchor_x = .5,
																			anchor_y = .5,
																			x = _W * .5,
																			y = -self.loginPanelHeight + _H * 0.07 * 2 ,
																			color = {0,0,0},
																			retranslator = true,
																			placeholder = T:translate("userName")
																		});
																		
			-- userPass Input
			self.interface.loginButton.userPassInput = self:createInput({
																			group = self.interface.loginButton,
																			width = _W * .8,
																			height = _H * .1,
																			anchor_x = .5,
																			anchor_y = .5,
																			x = _W * .5,
																			y = -self.loginPanelHeight + _H * 0.07 * 4,
																			color = {0,0,0},
																			retranslator = true,
																			placeholder = T:translate("userPass")
																		});
			
			-- enter button
			self.interface.loginButton.enterButton = self:createButton({	onEvent = 	function(event)
																				local p1, p2 = pcall(function()
																					if event.phase == "begin" then
																					
																					elseif event.phase == "release" or event.phase == "tap" or event.phase == "ended"  then
																						USER:signin({login = self.interface.loginButton.userNameInput.text, password = self.interface.loginButton.userPassInput.text});
																						self:openHideLoginPanel();
																						return true;
																					end
																					
																				end)
																				if(p1 == false) then
																					networkManager:send_client_error_to_server('StartScreenClass:createLoginInterface enterButtonListener', p2);
																				else
																					return p2;
																				end					
																			end,
																group = self.interface.loginButton,
																
																sheet = CONSTANT.buttonsImageSheet1,
																defaultFrame = 15,
																overFrame = 13,																
																
																title = T:translate('enter'),
																fontSize = 22 * CONSTANT.textScale,					
																
																scale = CONSTANT.longButtonScale, 
																scaleType = "x",
																
																anchor_x = .5,
																anchor_y = .5,
																x = _W * .5,
																y = -self.loginPanelHeight + _H * 0.07 * 6,
															});
		end)
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:createLoginInterface', p2);
		else
			return p2;
		end
	end		

	
	--[[
		* Function: openHideLoginPanel
		*
		*
		* @since  27.06.15
		* @author t
	]]
	function self:openHideLoginPanel()
		local p1, p2 = pcall(function()
			local y, title = 0, '';
			if self.interface.loginButton.open then
				y = 0;
				title = T:translate("login");
			else
				y = self.loginPanelHeight;
				title = T:translate("close");
			end
			self.interface.loginButton.open = not self.interface.loginButton.open;
			
			self.interface.loginButton.title.text = title;
			transition.to(self.interface.loginGroup, {time = 200, y = y});		
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:openHideLoginPanel', p2);
		else
			return p2;
		end
	end

	
	--[[
		* Function: progress
		*
		*
		* @since  25.12.14
		* @author t
	]]
	function self:progress(params)
		local p1, p2 = pcall(function()
			local percent = params.percent or 3;
			self.interface.progress.currentValue = self.interface.progress.currentValue + percent;
			if(self.interface.progress.currentValue > 100)then 
				self.interface.progress.currentValue = 100; 
			end
			self.interface.progress:setValue(self.interface.progress.currentValue / self.interface.progress.maxValue);
			self.interface.progress.interface.timerTitle.text = T:translate("loading:", {})..self.interface.progress.currentValue.."%";
			self.interface.progress.interface.timerTitle.x = self.interface.progress.interface.front.x + self.interface.progress.interface.timerTitle.contentWidth * 0.5;		
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:progress', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: set text
		*
		*
		* @since  25.12.14
		* @author t
	]]
	function self:setText(params)
		local p1, p2 = pcall(function()
			local text = self.interface.progress.currentValue;
			local sign = "%";
			
			if params.text then
				text = params.text
				sign = " bytes";
			end
			

			self.interface.progress.interface.timerTitle.text = T:translate("loading:", {})..text..sign;
			self.interface.progress.interface.timerTitle.x = self.interface.progress.interface.front.x + self.interface.progress.interface.timerTitle.contentWidth * 0.5;		
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('StartScreenClass:progress', p2);
		else
			return p2;
		end
	end	
	
	
	--[[
		* Function: listner for enter frame which set new value
		*
		* @event:	
		*
		*
		*
		* @since  11.02.14
		* @author pcemma, tooreckiy
	]]
	self.enterFrame = function(event)
		if(self and table.objectSize(self) ~= 0 and self.interface.progress.currentValue < 100)then
			self:progress({})
		else
			Runtime:removeEventListener("enterFrame", self.enterFrame);
		end
	end
	
	return self:__construct(params);
end

return StartScreenClass;