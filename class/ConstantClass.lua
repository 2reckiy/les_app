print("CLASS ConstantClass IS CONNECTED");
local ConstantClass = {};

function ConstantClass.new(params)
	local self = {};
	
	self.serverDiffTime = 3600;
	
	self.settings = {};
	
	self.textScale = _W / 640;

	self.headerIndent = _H * 0.001;
	self.headerHeight = _W * 0.121;
	self.bottomHeight = _H * 0.1;


	-- КНОПКИ
	self.longButtonScale = _W * 0.596
	self.smallButtonScale = _W * 0.343;
	self.littleButtonScale = _W * 0.328;
	self.standartButtonHeight = _W * 0.103;
	self.ButtonScale301x66 = _W * 0.470;


	--РАСПОЛОЖЕНИЕ КНОПОК ОТНОСИТЕЛЬНО ЦЕНТРА ЭКРАНА (МАЛЕНЬКАЯ - БОЛЬШАЯ)
	self.leftButtonPosition = _W * 0.196;
	self.rightButtonPosition = _W * 0.675;


	--РАСПОЛОЖЕНИЕ КНОПОК ОТНОСИТЕЛЬНО ЦЕНТРА ЭКРАНА (ПРОГРЕСС БАР - МАЛЕНЬКАЯ)
	self.leftProgressBarPosition = _W * 0.325;
	self.rightProgressButtonPosition = _W * 0.804;

	-- РАСПОЛОЖЕНИЕ КНОПОК ШИРИНОЙ 301 НА ЭКРАНЕ
	self.leftButton301x66Position = _W * 0.26;
	self.rightButton301x66Position = _W * 0.739;

	
	-- COLORS
	self.lightBlueTextColor = {0.76, 0.772, 0.952};
	self.greenTextColor = {0.69, 0.847, 0.094};
	self.redTextColor = {0.717, 0.29, 0.274};
	self.lightGreyTextColor = {0.32,0.30,0.29};
	self.greyTextColor = {0.364,0.364,0.364};

	
	-- ИНПУТЫ
	self.inputRetranaltorHeight = _W * 0.0875;
	
	self.constantSettings = {
					globalDataVersion = _GLOBAL_DATA_VERSION,
					volume = 100,
					music = 1,
					effects = 1,
					notification = 1,
					saveBuyByGoldPopup = 1
				};
	
	
	--[[
		*  Function: конструктор класса
		*
		* @params:	
		*	
		*
		*
		* @since  09.10.14
		* @author pcemma
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()

			self:getSettings();
			
			self:createButtonsImageSheets();
			
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:__construct', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: Вытягивает из файла настроек все параметры
		*
		* @params:	
		*	
		*
		*
		* @since  09.10.14
		* @author pcemma
	]]
	function self:getSettings()
		local p1, p2 = pcall(function()
			local path = system.pathForFile( "settings", system.DocumentsDirectory);
			local fh, errStr = io.open( path, "r" );
			if(fh)then
				self.settings = json.decode(fh:read( ));
				io.close( fh );
				fh = nil;
				
				self:chaeckNewSettings();
			else
				self.settings = self.constantSettings;
				self:setSettings();
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:getSettings', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: Записывает в файл настроек все параметры
		*
		* @params:	
		*	
		*
		*
		* @since  09.10.14
		* @author pcemma
	]]
	function self:setSettings()
		local p1, p2 = pcall(function()
			local path = system.pathForFile( "settings", system.DocumentsDirectory );
			local fh, errStr = io.open( path, "w" );
			fh:write( json.encode(self.settings) );
			io.close( fh );
			fh = nil;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:setSettings', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		*  Function: Проверяет сетиингы на наличие новых настроек, если таковы есть
		*
		* @params:	
		*	
		*
		*
		* @since  10.03.15
		* @author pcemma
	]]
	function self:chaeckNewSettings()
		local p1, p2 = pcall(function()
			local findNewSettingFlag = false;
			for i,v in pairs(self.constantSettings)do
				if(self.settings[i] == nil)then
					self.settings[i] = self.constantSettings[i];
					findNewSettingFlag = true;
				end
			end
			
			if(findNewSettingFlag)then
				self:setSettings();
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:chaeckNewSettings', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: 
		
		* @params:	
		
		
		
		* @since  03.02.14
		* @author pcemma, tooreckiy
	]]
	function self:updateFromServer()
		local p1, p2 = pcall(function()
			if(DATA.constants)then
				for i,v in pairs(DATA.constants) do
					self[i] = v;
				end
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:updateFromServer', p2);
		else
			return p2;
		end
	end


	--[[
		* Function: 
		
		* @params:	
		
		
		
		* @since  03.02.14
		* @author pcemma, tooreckiy
	]]
	function self:add(params)
		local p1, p2 = pcall(function()
			if(not self[params.name])then
				
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:add', p2);
		else
			return p2;
		end
	end

	
	--[[
		* Function: check is trap enable
		*
		* @params: - array	{
								object - obj, newText object,
								width - int, max width for title 
							}
		*				
		*
		*
		*
		* @since  30.09.14
		* @author tooreckiy
	]]
	function self:replaceTitleWithDots(params)
		local p1, p2 = pcall(function()
			local limit = string.length(params.object.text);
			-- local count = 0;
			-- local koef = 1;			
			-- if T.lang == 'ru' then
				-- koef = 2;
			-- end
			if(params.object.contentWidth > params.width)then
				while  params.object.contentWidth > params.width do
					-- local tempKoef = koef;
				
					-- if params.object.text:sub(params.object.text:len()) == " "  then
						-- tempKoef = 1;
					-- end	
					
					-- params.object.text = string.sub( params.object.text, 1, string.len( params.object.text ) - 1*tempKoef );
					
					-- count = count + 1;
					-- if count > 50 then
						-- break;
					-- end
					
					
					params.object.text = string.cut(params.object.text, limit - 1);
					limit = limit - 1;
				end
				
				-- for i=1,3 do
					-- local tempKoef = koef;
					-- if params.object.text:sub(params.object.text:len()) == " "  then
						-- tempKoef = 1;
					-- end			
					-- params.object.text = string.sub( params.object.text, 1, string.len( params.object.text ) - 1*tempKoef );
				-- end
				params.object.text = string.cut(params.object.text, limit - 3);
				params.object.text = params.object.text.."...";
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:replaceTitleWithDots', p2);
		else
			return p2;
		end
	end	

	
	--[[
		* Function: Собирает глобальный массив данных о игре. Обновляет файл с глобальными данными
		*
		* @params: - array	{
		*						globalDataVersion - int, number of the cuurent version of the data on server,
		*						globalData - array, array of the current DATA on server (*) 
		*					}
		*				
		*
		*
		*
		* @since  10.10.14
		* @author pcemma
	]]
	function self:getGlobalData(params)
		local p1, p2 = pcall(function()
			local DATA = {};
			print(">>>>>>>>");
			print("version from server: ", params.globalDataVersion);
			print("version from settings file: ", self.settings.globalDataVersion);
			print("version from _GLOBAL_DATA_VERSION: ", _GLOBAL_DATA_VERSION);
			
			local tempVer = CONSTANT.settings.globalDataVersion;
			if(tempVer < _GLOBAL_DATA_VERSION)then
				tempVer = _GLOBAL_DATA_VERSION;
			end
			
			
			if(tonumber(params.globalDataVersion) > tonumber(tempVer) or _DEBUG)then
				print("WE have old data version");
				self.settings.globalDataVersion = params.globalDataVersion;
				self:setSettings();
				local objSize = table.objectSize(params.globalData);
				for i,v in pairs(params.globalData) do
					DATA[i] = v;
				end
				-- перезаписать файл
				print("SET NEW data to file");
				local path = system.pathForFile("globalData", system.DocumentsDirectory);
				local fh, errStr = io.open( path, "w" );
				fh:write( json.encode(DATA) )
				io.close( fh );
				fh = nil;
			else
				print("WE have normal data version");
				if(_GLOBAL_DATA_VERSION > CONSTANT.settings.globalDataVersion)then
					self.settings.globalDataVersion = _GLOBAL_DATA_VERSION;
					self:setSettings();
					
					print("_GLOBAL_DATA_VERSION > CONSTANT.settings.globalDataVersion SO read data from root file");
					local systemPath = system.pathForFile("globalData", system.ResourceDirectory);
					local systemFh, errStr = io.open( systemPath, "r" );
					DATA = json.decode(systemFh:read());
					io.close( systemFh );
					systemFh = nil;
					
					print("_GLOBAL_DATA_VERSION > CONSTANT.settings.globalDataVersion SO set data to client file");
					local path = system.pathForFile("globalData", system.DocumentsDirectory);
					local fh, errStr = io.open( path, "w" );
					fh:write( json.encode(DATA) )
					io.close( fh );
					fh = nil;
				else
					-- взять данные из файла. Проверяем есть ли файл у юзера. Если нет, то создаем новый и туда записываем данные из файла из корня игры
					local path = system.pathForFile("globalData", system.DocumentsDirectory);
					local fh, errStr = io.open( path, "r" );
					if(fh)then
						print("read data from temp file on client");
						DATA = json.decode(fh:read( ));
						io.close( fh );
						fh = nil;
					else
						print("NO TEMP FILE read data from root file");
						local systemPath = system.pathForFile("globalData", system.ResourceDirectory);
						local systemFh, errStr = io.open( systemPath, "r" );
						DATA = json.decode(systemFh:read( ));
						io.close( systemFh );
						systemFh = nil;
						
						print("set data to client file");
						local path = system.pathForFile("globalData", system.DocumentsDirectory);
						local fh, errStr = io.open( path, "w" );
						fh:write( json.encode(DATA) )
						io.close( fh );
						fh = nil;
					end
				end
			end
			print("<<<<<<<<");
			return DATA;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:getGlobalData', p2);
		else
			return p2;
		end
	end	


	--[[
		* Function: переводит либо на приложение в маркетах, либо на сайт
		*
		*
		*
		* @since  26.01.15
		* @author pcemma
	]]
	function self:goToWebStore(url)
		local p1, p2 = pcall(function()
			if url and url ~= "" then
				system.openURL(url);
			else
				system.openURL( "http://crystalrush.net" );
			end	
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:goToWebStore', p2);
		else
			return p2;
		end
	end	

	
	--[[
		* Function: создает imagesheets для кнопок
		*
		*
		*
		* @since  19.02.15
		* @author pcemma
	]]
	function self:createButtonsImageSheets()
		local p1, p2 = pcall(function()
			local options = {
				frames =
				{
					{ x = 0, 	y = 0, 	 width = 301, height = 66 }, -- 1 301x66BlueButton.png
					{ x = 301, 	y = 0, 	 width = 140, height = 66 }, -- 2 140x66BlueButton.png
					{ x = 0, 	y = 66,  width = 301, height = 66 }, -- 3 301x66PressedButton.png
					{ x = 301, 	y = 66,  width = 140, height = 66 }, -- 4 140x66PressedButton.png
					{ x = 0, 	y = 132, width = 301, height = 66 }, -- 5 301x66GreenButton.png
					{ x = 301, 	y = 132, width = 140, height = 66 }, -- 6 140x66GreenButton.png
					{ x = 0, 	y = 198, width = 382, height = 66 }, -- 7 longPressedButton.png
					{ x = 0, 	y = 264, width = 382, height = 66 }, -- 8 longGreenButton.png
					{ x = 0, 	y = 330, width = 382, height = 66 }, -- 9 longDarkBlueButton.png
					{ x = 0, 	y = 396, width = 382, height = 66 }, -- 10 longWhiteButton.png
					{ x = 0, 	y = 462, width = 210, height = 48 }, -- 11 smallGreenButton.png
					{ x = 210, 	y = 462, width = 210, height = 48 }, -- 12 smallButtonPressed.png
					
					{ x = 382, 	y = 198, width = 220, height = 66 }, -- 13 pressedButton.png
					{ x = 382,  y = 264, width = 220, height = 66 }, -- 14 greenButton.png
					{ x = 382, 	y = 330, width = 220, height = 66 }, -- 15 darkBlueButton.png
					{ x = 382,  y = 396, width = 220, height = 66 }, -- 16 whiteButton.png
					
					{ x = 420,  y = 462, width = 210, height = 48 }  -- 17 smallBlueButton.png
				},
				sheetContentWidth = 1024,
				sheetContentHeight = 512
			}
			self.buttonsImageSheet1 = graphics.newImageSheet( "images/interface/buttonSheet1.png", options );
			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('ConstantClass:createButtonsImageSheets', p2);
		else
			return p2;
		end
	end	

	
	return self:__construct(params);
end

return ConstantClass;
	
	
	
	