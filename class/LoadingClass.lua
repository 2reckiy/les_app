print("CLASS LoadingClass IS CONNECTED");


local LoadingClass = {};

function LoadingClass.new()
	local self = GameDesignClass.new({});

	--[[
		* Function: CONSTRACT
		*
		*	
		*
		*
		* @since  12.09.14
		* @author t
	]]
	function self:__construct()
		local p1, p2 = pcall(function()
			self.flag = false;

			self.interface = {};
			self.interface.group = display.newGroup();
			self.interface.group.alpha = 0;
			self.interface.background = self:createRect({group = self.interface.group, x = _W/2, y = _H/2, width = _W, height = _H, color = {0, 0, 0}, alpha = 0.6});
			self.interface.background:addEventListener("touch", function(event) return true; end);
			
			local options = {
				width = 128,
				height = 128,
				numFrames = 4,
				sheetContentWidth = 512,
				sheetContentHeight = 128
			}
			local spinnerSingleSheet = graphics.newImageSheet( "images/loading.png", options );

			-- Create the widget
			self.interface.spinner = widget.newSpinner
			{
				x = _W * 0.5,
				y = _H * 0.5,
				width = 128,
				height = 128,
				sheet = spinnerSingleSheet,
				startFrame = 1,
				deltaAngle = 10,
				incrementEvery = 20
			}
			self.interface.group:insert(self.interface.spinner);
			
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('LoadingClass:loading', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: EMERGENCY SHUTDOWN
		*
		*	
		*
		*
		* @since  12.09.14
		* @author t
	]]
	function self.emergencyShutdown()
		local p1, p2 = pcall(function()
			self.flag = false;

			-- native.setActivityIndicator( self.flag );
			if(self and self.interface and self.interface.group.alpha == 1)then
				self.interface.group.alpha = 0;
				self.interface.spinner:stop();
			end
			
			if self.loadingTimer then
				timer.cancel(self.loadingTimer);
			end			
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('LoadingClass.emergencyShutdown', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: SET LOADING
		*
		* @flag: bool, show\hide loading	
		*	
		*
		*
		* @since  12.09.14
		* @author t
	]]
	function self:loading(flag)
		local p1, p2 = pcall(function()
			self.flag = flag;
			-- print("loading", self.flag);
			-- native.setActivityIndicator( self.flag );
			if self.loadingTimer then
				timer.cancel(self.loadingTimer);
			end
			
			if(self.flag)then
				if(self.interface.group.alpha == 0)then
					self.interface.group.alpha = 1;
					self.interface.spinner:start();
					self.interface.group:toFront();
				end
				self.loadingTimer = timer.performWithDelay( 5000, self.emergencyShutdown );
			else
				if(self.interface.group.alpha == 1)then
					self.interface.group.alpha = 0;
					self.interface.spinner:stop();
				end
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('LoadingClass:loading', p2);
		else
			return p2;
		end
	end
	
	return self:__construct();
	
end


return LoadingClass;