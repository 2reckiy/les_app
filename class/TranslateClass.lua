print("CLASS TranslateClass IS CONNECTED");


local TranslateClass = {};

function TranslateClass.new(params)
	
	local self = {};
	
	
	--[[
		* Function: конструктор класса
		*
		* @params:	
		*	
		*
		*
		* @since  14.04.14
		* @author pcemma, tooreckiy
	]]
	function self:__construct(params)
		local p1, p2 = pcall(function()

			self.lang = "ru";
			-- if(system.getPreference("locale", "language") ~= "ru")then
				-- self.lang = "en";
			-- else
				-- self.lang = "ru";
			-- end

			self:addLanguage();
			return self;
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('TranslateClass:__construct', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: главная Function перевода
		*
		* @params:	
		*	@key:		int, the name of the string wich need to translate
		*	@params:	array, array of different values if need for translate
		*	
		*
		*
		* @since  14.04.14
		* @author pcemma, tooreckiy
	]]
	function self:translate(key, params)
		local p1, p2 = pcall(function()
			return self[self.lang](key, params);
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('TranslateClass:translate', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: Добавления языка
		*
		* @params:	
		*	
		*
		*
		* @since  14.04.14
		* @author pcemma, tooreckiy
	]]
	function self:addLanguage()
		local p1, p2 = pcall(function()
			if(not self[self.lang]) then
				self[self.lang] = require('languages.'..self.lang);
			end
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('TranslateClass:addLanguage', p2);
		else
			return p2;
		end
	end
	
	
	--[[
		* Function: смены языка
		*
		* @params:	
		*	@lang:		str, the new language
		*
		*
		* @since  14.04.14
		* @author pcemma, tooreckiy
	]]
	function self:changeLanguage(lang)
		local p1, p2 = pcall(function()
			self.lang = lang;
			self:addLanguage();
		end);
		if(p1 == false) then
			networkManager:send_client_error_to_server('TranslateClass:changeLanguage', p2);
		else
			return p2;
		end
	end
	

	
	
	return self:__construct(params);
end

return TranslateClass;