print( "ORIENTATION: "..system.orientation )

local launchArgs = ...

-- hide device status bar
display.setStatusBar( display.HiddenStatusBar );
system.setIdleTimer( false )  -- disable (turn off) the idle timer

-- CONSTANTS AND VERIABLES
	require 'lib.constants';
	require 'lib.widgets.ext' ;
	
	client, ip, port = nil,nil,nil;
	
-- LIBRARYS
	socket = require 'socket';
	json = require 'json';
	require 'lib.lib';	
	
	
	-- deflate = require("deflatelua");
	
	-- personal config for developers
	require "config_data";
	
	widget = require 'widget';

	
	if (_DEBUG) then
		stat = require 'lib.stat';
		performance = stat.PerformanceOutput.new();
	
		-- eventConsole = require("eventConsole");
		-- console = eventConsole.eventConsole:new();
	end	

-- CLASSES
	
	-- require class root 
	RootClass = require 'class.RootClass';	
	Root = RootClass.new();
	-- INIT SUSPEND,KEY, NOTIFICATION LISTENERS
	Root:initRuntimeListeners();
	
	
	-- game design class
	GameDesignClass = require 'class.GameDesignClass';
	
	-- network class
	NetworkClass = require 'class.NetworkClass';
	networkManager = NetworkClass.new();	

	-- constants class
	ConstantClass = require 'class.ConstantClass';	
	CONSTANT = ConstantClass.new({});
	
	-- MUSIC CLASS
	MusicManagerClass = require 'class.MusicManagerClass';	
	musicManager = MusicManagerClass.new({});
	
	-- loader class
	LoadingClass = require 'class.LoadingClass';
	L = LoadingClass.new();

	-- localization class
	TranslateClass = require 'class.TranslateClass';
	T = TranslateClass.new();

	-- popup class
	PopupClass = require 'class.PopupClass';
				
	-- user class
	UserClass = require 'class.UserClass';	
	-- USER OBJECT
	USER = UserClass.new({});
	
	-- start screen class
	StartScreenClass = require 'class.StartScreenClass';	
	startScreen = StartScreenClass.new({});
	

	
