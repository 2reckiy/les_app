application =
{
    content =
    {
		fps = 30,
    },

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert"
            }
        },
		google =
		{
			-- This Project Number (also known as a Sender ID) tells Corona to register this application
			-- for push notifications with the Google Cloud Messaging service on startup.
			-- This number can be obtained from the Google API Console at:  https://code.google.com/apis/console
			 -- projectNumber = "983479691336",
		},		
    },
	
	license =
	{
		google =
		{
			-- key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuXNYGX2WbI7dAAKNsQDWdWKCZ+3bfhzUvo+RmEH3wBOHG6MetGZ0KixekK6fMXc4VXVX4gV5ZcWwqN0FA7ZqZesgg9NhTIgp/xwe5PbnO3ePm07OIEYHAvxpko8w+YwdCmXFqe+Gr9fVjiEHpRcCpA/VVnR3JT07oColtDhy0zDO1mgHXobnWN/9AFYEI85Y+Jp499vsVQe2cGDzHCUiMcrxQ6oqq6X/UDJaMtQwoW2xbx33rUkP54PDkRalU7hoStxLl7bF4h1i3f3l58gOE0T18TMI7AQjPwHJQUo0Y+ROMij7f02X9mncqMnE4mk3Rw5HliALpFC6i+A4SLUYCwIDAQAB",
		},
	},	
}

