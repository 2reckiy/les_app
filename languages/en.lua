local func = function( key, params )
	local l = {};
	if not params then
		params = {};
	end
	
	l.hello = "HEllo!";
	l.error = "Ошибка!";
	l.login = "Login";
	l.close = "Close";
	l.enter = "Enter";
	
	l.enteringTitle = "Коснитесь экрана!";
	l.userName = "User name";
	l.userPass = "User password";
	
	return l[key];
end

return func;


